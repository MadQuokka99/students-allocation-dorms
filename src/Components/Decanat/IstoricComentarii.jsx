import React, { Component } from "react";
import { Comment } from "semantic-ui-react";

class IstoricComentarii extends Component {
  render() {
    return (
      <div>
        {this.props.listaComentarii && this.props.listaComentarii.filter(x => x.tipcomentariu !== "automat").map((comm) => (
          <Comment key={comm.data}>
            <Comment.Content>
              <Comment.Author as="a">{comm.username}</Comment.Author>
              <Comment.Metadata>
                <div>{comm.data}</div>
              </Comment.Metadata>
              <Comment.Text>{comm.comentariu}</Comment.Text>
              <Comment.Actions>
                <Comment.Action>
                  {comm.tipcomentariu.toUpperCase()}
                </Comment.Action>
              </Comment.Actions>
            </Comment.Content>
          </Comment>
        ))}
      </div>
    );
  }
}

export default IstoricComentarii;