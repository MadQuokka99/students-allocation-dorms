import React, {Component} from "react";
import axiosRef from "../../../axios-AGSIS-API-refactor";

import {
  Dropdown,
  Grid,
  Accordion,
  List,
  Icon,
  Popup,
  Message,
  Header,
  Button,
  Modal,
  Checkbox,
  Loader,
  Container,
  Input,
  Segment,
  Label, Form, Radio,
} from "semantic-ui-react";
import {SemanticToastContainer} from "react-semantic-toasts";
import "react-semantic-toasts/styles/react-semantic-alert.css";

import Camera from "./Camera";
import "./ListaCamere.css";
import LabelStatistici from "../../LabelStatistici";

class ListaCamere extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false,

      idCaminSelectat: null,
      denumireCaminSelectat: null,

      listaEtaje: [],
      etajSelectat: null,

      listaCamerelong: [],
      listaCamere: null,
      listaFixaCamere: [],

      activeIndex: [], //pentru afisarea de tip Accordion a camerelor

      filtruCamereMF: null,

      studentSelectat: null,
      idCaminStudentSelectat: null,
      studenti: [], //doar studentii care au cazarea expirata

      modalOpen: false, //pentru afisarea Modal-ului de confirmare a stergerii studentilor care nu au confirmat
      modalOpenConfirmareDubla: false, //pentru afisarea Modal-ului de confirmare dubla a stergerii studentilor care nu au confirmat
      modalOpenModalCautare: false, //pentru afisarea Modal-ului de cautare a studentilor cazati

      checkboxCamLibere: false, //state pentru checkbox-ul "Afiseaza camerele neocupate complet"
      disableCheckboxCamLibere: true, //state pentru a stabili daca checkbox-ul de filtrarea camere neocupate complet este disabled sau nu

      studentiCazati: [],

      listaCamerePentruFunctieCautare: [],

      tipCameraMFCazare: "N",
      ID_CameraCazare: null,

      loaderListaCamere: false, //state pentru loaderul care apare atunci cand se face call-ul de camere, la selectarea caminului/etajului

      listaStudentiCautatiServerSide: [], //pentru retinerea listei de studenti returnati de functia de cautare studenti cazati
      numeStudentCautat: "", //pentru retinerea studentului cautat in modalul de cautare
      buttonCautaStudentServerSide: false,
      loaderListaStudenti: false,
    };

    this.selecteazaCamin = this.selecteazaCamin.bind(this);
  }

  /**
   * metoda preia caminul ales si stocheaza id-ul si denumirea
   */
  selecteazaCamin = (camin, etaj, camera) => {
    /**
     * pentru afisarea listei de camere corespunzatoare caminului si etajului studentului selectat din dropdown-ul de cautare
     */

    let caminStudentSelectat;
    if (camin.value === undefined) {
      caminStudentSelectat = camin;
      this.setState({
        idCaminSelectat: camin,
        etajSelectat: etaj,
      });
    } else {
      caminStudentSelectat = camin.value;
      this.setState({
        idCaminSelectat: camin.value,
        etajSelectat: null,
      });
    }

    /**
     * se preia lista de etaje
     */
    this.setState({
      listaEtaje: [],
      listaCamerelong: [],
      studentSelectat: null,
      activeIndex: [],
    });

    let listaCamineRef = this.props.listaCamine;
    let caminSelectat = listaCamineRef.filter(
      (el) => el.value === caminStudentSelectat
    );

    let listaEtajeRef = caminSelectat[0].etaje;
    let temp = [];

    listaEtajeRef.forEach((et) => {
      let etaj = {
        key: et.Etaj,
        text: "Etajul " + et.Etaj,
        value: et.Etaj,
      };
      temp.push(etaj);
    });

    this.setState({
      listaEtaje: temp,
      denumireCaminSelectat: caminSelectat[0].text,
      loaderListaCamere: true,
    });
    //console.log(this.state.listaEtaje); //afiseaza lista etajelor din caminul selectat

    let idFacultate = this.props.idFacultate;

    if (this.props.eSefAdminCamine) {
      idFacultate = -1;
    }
    /**
     * se preia lista de camere din caminul respectiv
     */
    axiosRef
      .get(
        `FacultateCamera/FacultateCameraCuStudentiCazatiListByFacultateAnUnivCamin?ID_Facultate=${idFacultate}&ID_AnUniv=${this.props.idAnCazare}&ID_Camin=${caminStudentSelectat}&ID_TipCazare=${this.props.ID_TipCazare}`
      )
      .then((response) => {
        let listaCamereRef = response.data;
        //console.log(response.data);

        this.setState({
          listaCamerelong: listaCamereRef, //toate informatiile despre toate camere
          listaFixaCamere: listaCamereRef, //lista care nu se va modifica in urma filtrarilor
          loaderListaCamere: false,
          disableCheckboxCamLibere: false,
          disableCheckboxToateCam: true,
          checkboxCamLibere: false,
        });

        /**
         *  pentru deschiderea camerei atunci cand este selectat un student din dropdown-ul de cautare
         */

        if (camin.value === undefined) {
          let listaCamereRefFiltrataEtaj = listaCamereRef.filter(
            (el) => el.Etaj === etaj
          );
          this.setState({
            listaCamerelong: listaCamereRefFiltrataEtaj,
          });

          //console.log(listaCamereRefFiltrataEtaj);

          listaCamereRefFiltrataEtaj.forEach((cam, index) => {
            if (cam.NumarCamera === camera) {
              this.setState({
                activeIndex: index,
              });
            }
          });
        }
      });
  };

  /**
   * metoda preia filtreaza lista de camere alocate dupa etajul selectat
   */
  selecteazaEtaj = (e, etaj) => {
    this.setState({
      checkboxCamLibere: false,
      disableCheckboxToateCam: false,
    });

    let etajSelectat;

    if (etaj.value === undefined) {
      etajSelectat = etaj;
    } else {
      etajSelectat = etaj.value;
    }

    /**
     * preia lista de camere alocate facultatii in caminul si etajul selectat si o stocheaza
     */

    let copieCamereRef = this.state.listaFixaCamere;
    let listaCamereRefFiltrataEtaj = copieCamereRef.filter(
      (el) => el.Etaj === etajSelectat
    );

    this.setState({
      listaCamerelong: listaCamereRefFiltrataEtaj, //toate informatiile despre toate camere
      etajSelectat: etaj.value,
    });

    //console.log(this.state.listaCamerelong); //afiseaza toate informatiile despre toate camerele
  };

  /**
   * metoda afiseaza doar camerele care mai au locuri libere atunci cand este bifat checkbox-ul "Afiseaza camerele neocupate complet"
   */
  afiseazaCamereNeocupateComplet = (e, {checked}) => {
    this.toggleCheckboxCamLibere();

    let camereFixe = this.state.listaFixaCamere;
    let camere = camereFixe;

    if (checked) {
      let camereFiltrate;

      if (this.props.eSefAdminCamine) {
        camereFiltrate = camere.filter(
          (el) => el.NrStudentiCazati < el.NrLocuri
        );
        this.setState({
          listaCamerelong: camereFiltrate,
        });
      } else {
        camereFiltrate = camere.filter(
          (el) => el.NrStudentiCazati < el.NrLocuriAlocateFacultate
        );
        this.setState({
          listaCamerelong: camereFiltrate,
        });
      }
    } else {
      this.setState({
        listaCamerelong: camereFixe,
      });
    }
  };

  /**
   * metoda modifica state-ul checkbox-ului de afisare lista camere neocupate complet din camin
   */
  toggleCheckboxCamLibere = () => {
    this.setState({
      checkboxCamLibere: !this.state.checkboxCamLibere,
    });
  };

  /**
   * metoda care permite afisarea camerelor de tip Accordion
   */
  handleClick = (e, titleProps) => {
    const {index} = titleProps;
    const {activeIndex} = this.state;
    const newIndex = activeIndex === index ? -1 : index;

    this.setState({activeIndex: newIndex});
  };

  /**
   * atunci cand este apelata, metoda seteaza Modal-ul de stergere a studentilor care nu au confirmat sa se deschida
   */
  handleOpen = () => this.setState({modalOpen: true});

  /**
   * atunci cand este apelata, metoda seteaza Modal-ul de stergere a studentilor care nu au confirmat sa se inchida
   */
  handleClose = () => this.setState({modalOpen: false});

  /**
   * atunci cand este apelata, metoda seteaza Modal-ul de confirmare dubla de stergere a studentilor care nu au confirmat sa se deschida
   */
  handleOpenConfirmareDubla = () =>
    this.setState({modalOpenConfirmareDubla: true});

  /**
   * atunci cand este apelata, metoda seteaza Modal-ul de confirmare dubla de stergere a studentilor care nu au confirmat sa se inchida
   */
  handleCloseConfirmareDubla = () =>
    this.setState({modalOpenConfirmareDubla: false, modalOpen: false});

  /**
   * atunci cand este apelata, metoda seteaza Modal-ul de cautare a studentilor care au primit confirmare sa se deschida
   */
  handleOpenModalCautare = () => {
    this.setState({modalOpenModalCautare: true});
  };

  /**
   * atunci cand este apelata, metoda seteaza Modal-ul de cautare a studentilor care au primit confirmare sa se inchida
   */
  handleCloseModalCautare = () =>
    this.setState({
      modalOpenModalCautare: false,
      numeStudentCautat: "",
      listaStudentiCautatiServerSide: [],
      buttonCautaStudentServerSide: false,
    });

  /** Filtre
   * Metodele seteaza filtrul ales de utilizator
   */
  filtreazaCamereMF = (value) => {
    this.setState({filtruCamereMF: value});
  };

  /**
   * metoda sterge cazarea studentilor care au facut cerere de precazare si au primit cazare, dar nu au confirmat (CazareConfirmata=false)
   */
  stergeStudentiiCareNuAuConfirmat = () => {
    axiosRef
      .get(
        `Cazare/CazareDeleteCazariNeconfirmate?ID_Facultate=${this.props.idFacultate}&ID_AnUnivCazare=${this.props.idAnCazare}`
      )
      .then((response) => {
        //console.log(response);
        this.handleCloseConfirmareDubla();
        window.location.reload();
      });
  };

  /**
   *
   * @param student
   * @param indiceCameraSauStudent - pentru a face distinctia intre apelarea functiei din Student sau din Camera se trimite 1 pentru apelarea din Student si 0 din Camera
   *
   * studentul este adaugat in lista studentilor cazati in camera cu ID_Camera specificat si se mareste numarul de studenti cazati al camerei respective
   * studentului i se stabileste CazareExpirataLaDataCurenta=false pentru a nu-l mai putea face draggable atunci cand este cazat in camera
   *
   * este apelata functia schimbaStatusStudent din Decanat, pentru a actualiza lista de studenti cazati ce vor fi afisati in dropdown-ul de cautare
   */

  schimbaStatusStudent = (student, indiceCameraSauStudent) => {
    //console.log(student);

    let listaCamere = [...this.state.listaCamerelong];
    for (let index in listaCamere) {
      if (listaCamere[index].ID_Camera === student.ID_CameraUltimaCazare) {
        listaCamere[index].NrStudentiCazati =
          listaCamere[index].NrStudentiCazati + 1;
        listaCamere[index].StudentiCazati.push(student);
        break;
      }
    }

    this.setState({
      listaCamerelong: listaCamere,
    });

    if (indiceCameraSauStudent === 0) {
      this.props.schimbaStatusStudent(student, indiceCameraSauStudent);
    }
  };

  /**
   *
   * @param student
   * @param camera
   *
   * studentul este scos din lista de studenti din camera si este actualizat numarul de studenti cazati in camera
   * se apeleaza functia adaugaStudentDecazat - studentul sters din lista de studenti cazati este adaugat in lista de studenti care au facut cerere de precazare, dar nu au primit cazare
   */
  scoateStudentDinCamera = (student, camera, response) => {
    let listaCamere = [...this.state.listaCamerelong];

    for (let i in listaCamere) {
      if (listaCamere[i].ID_Camera === camera.ID_Camera) {
        listaCamere[i].NrStudentiCazati = listaCamere[i].NrStudentiCazati - 1
        for (let j in listaCamere[i].StudentiCazati) {
          if (listaCamere[i].StudentiCazati[j].ID_Student === student.ID_Student) {
            listaCamere[i].StudentiCazati.splice(j, 1);
            break;
          }
        }
        break;
      }
    }

    this.props.adaugaStudentDecazat(student, response);
  };

  mutaStudent = (student, ID_CameraCurenta, ID_CameraNoua) => {

    let listaCamere = [...this.state.listaCamerelong];

    for (let i in listaCamere) {
      //se scoate studentul din camera curenta
      if (listaCamere[i].ID_Camera === ID_CameraCurenta) {
        listaCamere[i].NrStudentiCazati = listaCamere[i].NrStudentiCazati - 1;

        //daca se muta singurul student din camera trebuie actualizat si genul
        if (listaCamere[i].NrStudentiCazati === 0) {
          listaCamere[i].TipCameraMF = 'N';
        }

        for (let j in listaCamere[i].StudentiCazati) {
          if (listaCamere[i].StudentiCazati[j].ID_Student === student.ID_Student) {
            listaCamere[i].StudentiCazati.splice(j, 1);
            break;
          }
        }
      }
      //daca camera in care este mutat studentul este in caminul curent, studentul este adaugat in camera
      if (listaCamere[i].ID_Camera === ID_CameraNoua) {
        //daca camera e goala, se actualizeaza genul camerei cu genul studentului
        if (listaCamere[i].NrStudentiCazati === 0) {
          listaCamere[i].TipCameraMF = student.Sex;
        }

        listaCamere[i].NrStudentiCazati = listaCamere[i].NrStudentiCazati + 1;
        listaCamere[i].StudentiCazati.push(student);
      }
    }

    this.setState({listaCamerelong: listaCamere});
  }

  /**
   *
   * @param tipCameraMF
   * @param ID_Camera
   *
   * actualizeaza icon-ul de gen al camerei
   */
  actualizeazaTipCameraMF = (tipCameraMF, ID_Camera) => {
    let camere = [...this.state.listaCamerelong];
    for (let i in camere) {
      if (camere[i].ID_Camera === ID_Camera) {
        camere[i].TipCameraMF = tipCameraMF;
        break;
      }
    }
    this.setState({listaCamerelong: camere});
  };

  cautaStudentServerSide = (
    nume,
    ID_AnUniv,
    ID_Facultate,
    numarInregistrariMaximDeReturnat = 10
  ) => {
    this.setState({
      loaderListaStudenti: true,
    });
    let idFacultate;
    if (this.props.eSefAdminCamine) {
      idFacultate = -1;
    } else {
      idFacultate = ID_Facultate;
    }

    axiosRef
      .get(
        `Cazare/CazareCautaDupaNumeStudent?nume=${nume}&ID_AnUniv=${this.props.idAnCazare}&numarInregistrariMaximDeReturnat=${numarInregistrariMaximDeReturnat}&ID_Facultate=${idFacultate}`
      )
      .then((r) => {
        //console.log(r);
        this.setState({
          listaStudentiCautatiServerSide: r.data,
          loaderListaStudenti: false,
        });
      })
      .catch((e) => {
        alert("A aparut o eroare pe server :)");
        console.log(e);
      });
    this.setState({
      buttonCautaStudentServerSide: true,
    });
  };

  render() {
    let camere = this.state.listaCamerelong;
    const activeIndex = this.state.activeIndex;

    /**
     *  are loc filtrarea camerelor in functie de optiunea aleasa (fete, baieti sau nespecificat)
     */
    if (this.state.filtruCamereMF !== null)
      camere = camere.filter((c) => c.TipCameraMF === this.state.filtruCamereMF);


    return (
      /**
       * partea din pagina Decanat alocata camerelor este impartita la randul sau in 6 coloane si 3 randuri
       * 1. dropdown-urile, checkoxul de afisare camere neocupate complet,
       * filtrele, butonul de cautare a studentilor cazati deja și butonul de stergere a studentilor care nu au confirmat
       * 2. labels cu raportul numarului de locuri libere pe gen
       * 3. afisarea tip lista cu Accordion a camerelor
       */

      /** Dropdown 1
       * Dropdown ce afiseaza lista de camine in care facultatea are alocate camere
       * odata selectata optiunea, se apeleaza metoda selecteazaCamin si se afiseaza toate camerele alocate facultatii din caminul respectiv
       */

      /** Dropdown 2
       * Dropdown ce afiseaza lista de etaje din caminul selectat
       * odata selectata optiunea, se apeleaza metoda selecteazaEtaj care filtreaza camerele afisate dupa etajul selectat
       */

      /** Checkbox
       * Checkbox-ul "Afiseaza camerele neocupate complet" permite utilizatorului afisarea fie a tuturor camerelor din caminul/etajul selectat
       * fie doar afisarea camerelor care mai au locuri neocupate
       * avantajul acestui filtru este de a fi mai usor de vizualizat pentru utilizator in care camere mai pot fi cazati studenti
       */

      /** Filtre
       *  Cele trei optiuni filtreaza camerele in functie de tipul camerei: fete, baieti sau gen nespecificat
       */

      /** Buton
       * Butonul "Studenti cazati" deschide un modal in care se afla un dropdown cu functie de cautare a studentilor cazati
       * Selectarea unui student din dropdown va activa un toast de informare cu datele studentului cautat
       * si va deschide camera in care este studentul cazat (prin apelarea functiei selecteazaCamin)
       */

      /** Buton
       *  prin apasarea butonului se deschide un Modal de confirmare a stergerii studentilor care nu au confirmat cazarea
       *  -> DA -> se deschide un al doilea Modal de reconfirmare -> DA -> studentii sunt stersi din baza de date
       *                                                        -> NU -> iesire din ambele Modale
       *  -> NU -> se inchide Modal-ul
       */

      /** Labels
       * Sub banner-ul cu elementele mai sus mentionate sunt trei labels, fiecare actualizandu-se in functie de numarul de locuri pe fiecare gen care este alocat in baza de date
       */

      <Grid rows={3} columns="equal">
        <SemanticToastContainer position="top-right"/>

        <Grid.Row
          className="banner"
          style={{
            backgroundColor: this.props.culoareFacultate,
          }}
          verticalAlign="middle"
          columns={6}
        >
          <Grid.Column className="bannerColumn">
            <Dropdown
              className="bannerDropdown"
              button
              floating
              fluid
              search
              selection
              value={this.state.idCaminSelectat}
              onChange={(e, camin) => this.selecteazaCamin(camin)}
              options={this.props.listaCamine}
              placeholder="Cămin"
            />
          </Grid.Column>


          <Grid.Column className="bannerColumn">
            <Dropdown
              button
              className="bannerDropdown"
              floating
              fluid
              search
              selection
              value={this.state.etajSelectat}
              onChange={this.selecteazaEtaj.bind(this)}
              options={this.state.listaEtaje}
              placeholder={"Etaj"}
            />
          </Grid.Column>

          <Grid.Column className="bannerColumn">
            <Popup
              trigger={
                <Button
                  type="button"
                  className="butonFiltru"
                  icon="filter"
                  content="FILTRE CAMERE"
                />
              }
              hoverable
              position="bottom left"
              flowing
              basic
            >
              <Form>
                <Form.Group grouped>
                  <strong>Locuri libere</strong>
                  <Form.Field>
                    <Checkbox
                      label="Afișează doar camerele cu locuri libere"
                      onClick={this.afiseazaCamereNeocupateComplet.bind(this)}
                      disabled={this.state.disableCheckboxCamLibere}
                      checked={this.state.checkboxCamLibere}
                    />
                  </Form.Field>
                </Form.Group>

                <Form.Group grouped>
                  <strong>Tipul camerei</strong>
                  <Form.Field>
                    <Radio
                      label='Camere de fete'
                      name='genRadioGroup'
                      disabled={this.state.disableCheckboxCamLibere}
                      checked={this.state.filtruCamereMF === 'F'}
                      onClick={() => this.filtreazaCamereMF("F")}
                    />
                  </Form.Field>
                  <Form.Field>
                    <Radio
                      label='Camere de băieți'
                      name='genRadioGroup'
                      disabled={this.state.disableCheckboxCamLibere}
                      checked={this.state.filtruCamereMF === 'M'}
                      onClick={() => this.filtreazaCamereMF("M")}
                    />
                  </Form.Field>
                  <Form.Field>
                    <Radio
                      label='Camere nealocate unui gen'
                      name='genRadioGroup'
                      disabled={this.state.disableCheckboxCamLibere}
                      checked={this.state.filtruCamereMF === 'N'}
                      onClick={() => this.filtreazaCamereMF("N")}
                    />
                  </Form.Field>
                  <Form.Field>
                    <Radio
                      label='Toate camerele'
                      name='genRadioGroup'
                      disabled={this.state.disableCheckboxCamLibere}
                      checked={this.state.filtruCamereMF == null}
                      onClick={() => this.filtreazaCamereMF(null)}
                    />
                  </Form.Field>
                </Form.Group>
              </Form>

            </Popup>
          </Grid.Column>


          <Grid.Column className="bannerColumn">
            <Modal
              size="large"
              trigger={
                <Button
                  type="button"
                  className="butonFiltru"
                  content="Caută studenți"
                  compact
                  size="small"
                  onClick={this.handleOpenModalCautare}
                  icon="search"
                />
              }
              closeIcon
              open={this.state.modalOpenModalCautare}
              onClose={this.handleCloseModalCautare}
            >
              <Modal.Header>
                <Icon name="search"/>
                Caută student
              </Modal.Header>
              <Modal.Content>
                <div className="inputSearchStudentiCazati">
                  <Input
                    className="cautare"
                    placeholder="Introdu numele de familie al studentului"
                    action
                    type="text"
                  >
                    <input
                      autoFocus={true}
                      onKeyPress={(e) => {
                        //daca se apasa enter se initiaza cautarea
                        //console.log(e.key);
                        if (e.key === "Enter") {
                          this.cautaStudentServerSide(
                            this.state.numeStudentCautat,
                            this.props.idAnCazare,
                            this.props.idFacultate
                          );
                        }
                      }}
                      onChange={(e, value) => {
                        this.setState({
                          numeStudentCautat: e.target.value,
                          buttonCautaStudentServerSide: false,
                        });
                      }}
                    />
                    <Button
                      type="button"
                      onClick={() => {
                        this.cautaStudentServerSide(
                          this.state.numeStudentCautat,
                          this.props.idAnCazare,
                          this.props.idFacultate
                        );
                      }}
                    >
                      Caută student
                    </Button>
                  </Input>
                </div>
                <List celled>
                  {this.state.loaderListaStudenti ? (
                    <Container textAlign="center">
                      <Loader active inline size="small"/>
                    </Container>
                  ) : this.state.listaStudentiCautatiServerSide.length === 0 &&
                  this.state.numeStudentCautat !== "" ? (
                    <div className="inputSearchStudentiCazati">
                      <Message
                        hidden={!this.state.buttonCautaStudentServerSide}
                        info
                        compact
                      >
                        Nu a fost găsit niciun student!
                      </Message>
                    </div>
                  ) : (
                    this.state.listaStudentiCautatiServerSide.map((student) => (
                      <Popup
                        key={student.ID_Student}
                        disabled={student.CazareExpirataLaDataCurenta}
                        content="Apasă pe student pentru a-l vedea în cameră!"
                        trigger={
                          <List.Item
                            onClick={() => {
                              if (
                                student.Etaj !== null &&
                                student.Etaj !== ""
                              ) {
                                this.selecteazaCamin(
                                  student.ID_Camin,
                                  student.Etaj,
                                  student.NumarCamera
                                );
                                this.handleCloseModalCautare();
                              }
                            }}
                          >
                            <Icon
                              name={
                                student.CazareExpirataLaDataCurenta === true
                                  ? "male"
                                  : student.CazareConfirmata
                                  ? "check"
                                  : !student.CazareConfirmata
                                    ? "x"
                                    : "x"
                              }
                              color={
                                student.CazareExpirataLaDataCurenta === true
                                  ? "grey"
                                  : student.CazareConfirmata
                                  ? "green"
                                  : !student.CazareConfirmata
                                    ? "red"
                                    : "black"
                              }
                            />
                            <List.Content>
                              {
                                <List.Header>
                                  {student.NumeIntreg}
                                  {
                                    student.DenumireFacultate === null &&
                                    <Popup trigger={
                                      <Icon name="exclamation circle" color="red"/>}
                                           content="Studentul nu are grupă și nu poate fi cazat"
                                    />
                                  }
                                  {` - ${student.Username}`}
                                </List.Header>
                              }

                              {
                                student.CazareExpirataLaDataCurenta === false
                                  ? `${student.DenumireCamin} - Camera ${student.NumarCamera}`
                                  : "Nu este cazat"
                              }

                              {  `    Tip cazare: ${student.DenumireTipCazare && student.DenumireTipCazare}`                              }

                            </List.Content>
                          </List.Item>
                        }
                      />
                    ))
                  )}
                </List>
              </Modal.Content>
              <Modal.Actions>
                <Segment floated="left" basic>
                  <Label>
                    <Icon name="check" color="green"/> Cazare confirmată
                  </Label>

                  <Label>
                    <Icon name="x" color="red"/> Cazare neconfirmată
                  </Label>
                  <Label>
                    <Icon name="male" color="grey"/> Nu este cazat
                  </Label>
                </Segment>
                <Button
                  style={{
                    marginTop: "auto",
                    marginBottom: "auto",
                  }}
                  type="button"
                  basic
                  color="red"
                  onClick={this.handleCloseModalCautare}
                >
                  Închide
                </Button>
              </Modal.Actions>
            </Modal>
          </Grid.Column>

          <Grid.Column className="bannerColumn">
            <Modal
              size="tiny"
              trigger={
                <Popup
                  content="Ștergerea studenților care nu au confirmat cazarea"
                  size="small"
                  position="bottom right"
                  trigger={
                    <Button
                      type="button"
                      className="butonFiltru"
                      content="Studenți neconfirmați"
                      compact
                      size="small"
                      color="red"
                      onClick={this.handleOpen}
                      icon="delete"
                    />
                  }
                />
              }
              closeIcon
              open={this.state.modalOpen}
              onClose={this.handleClose}
            >
              <Header block icon="sign-out" content="Decazare"/>
              <Modal.Content>
                <p>Dorești să decazezi studenții care nu au confirmat locul?</p>
              </Modal.Content>
              <Modal.Actions>
                <Button
                  type="button"
                  basic
                  color="red"
                  onClick={this.handleClose}
                >
                  <Icon name="remove"/> Nu
                </Button>
                <Button
                  type="button"
                  color="green"
                  inverted
                  onClick={this.handleOpenConfirmareDubla}
                >
                  <Icon name="checkmark"/> DA
                </Button>
              </Modal.Actions>
            </Modal>

            <Modal
              size="tiny"
              closeIcon
              open={this.state.modalOpenConfirmareDubla}
              onClose={this.handleCloseConfirmareDubla}
            >
              <Header block icon="sign-out" content="Decazare"/>
              <Modal.Content>
                <p>
                  Această acțiune va șterge toți studenții care nu au confirmat
                  locul, este făcută special pentru trecerea de la faza I la
                  faza II și de la faza II la faza III, ești sigur/ă că vrei să
                  continui?
                </p>
              </Modal.Content>
              <Modal.Actions>
                <Button
                  type="button"
                  basic
                  color="red"
                  onClick={this.handleCloseConfirmareDubla}
                >
                  <Icon name="remove"/> Nu
                </Button>
                <Button
                  type="button"
                  color="green"
                  inverted
                  onClick={this.stergeStudentiiCareNuAuConfirmat}
                >
                  <Icon name="checkmark"/> DA
                </Button>
              </Modal.Actions>
            </Modal>
          </Grid.Column>
        </Grid.Row>

        <Grid.Row>
          <Grid.Column className="labelCazare">
            <LabelStatistici
              icon="female"
              title="Locuri libere fete:"
              detail={this.props.nrLocuriLibereFete}
              culoareFacultate={"magenta"}
            />
          </Grid.Column>
          <Grid.Column className="labelSeparatorCazare"/>
          <Grid.Column className="labelCazare">
            <LabelStatistici
              icon="male"
              title="Locuri libere băieți:"
              detail={this.props.nrLocuriLibereBaieti}
              culoareFacultate={"blue"}
            />
          </Grid.Column>
          <Grid.Column className="labelSeparatorCazare"/>
          <Grid.Column className="labelCazare">
            <LabelStatistici
              icon="male"
              title="Locuri libere nespecificat:"
              detail={this.props.nrLocuriLibereNespecificat}
              culoareFacultate={"gray"}
            />
          </Grid.Column>
        </Grid.Row>

        {this.state.loaderListaCamere ? (
          <Container textAlign="center">
            <Loader active inline size="small"/>
          </Container>
        ) : camere.length === 0 ? (
          /**
           * daca la etajul respectiv nu sunt alocate camere corespunzatoare genului ales ca filtru, se afiseaza un mesaj
           * daca nu este selectat niciun filtru, va fi afisat mesajul "Nu ați selectat căminul și/sau etajul"
           */
          <Grid.Row centered>
            <div>
              {this.state.baietiIcon === "blue male large icon" ? (
                <Message info>
                  {" "}
                  Nu există camere alocate pentru băieți pe acest etaj{" "}
                </Message>
              ) : this.state.feteIcon === "pink female large icon" ? (
                <Message info>
                  {" "}
                  Nu există camere alocate pentru fete pe acest etaj{" "}
                </Message>
              ) : this.state.nespecificatIcon === "black male large icon" ? (
                <Message info>
                  {" "}
                  Nu există camere alocate pe acest etaj a căror gen nu a fost
                  specificat încă{" "}
                </Message>
              ) : (
                <Message info> Nu ați selectat căminul</Message>
              )}
            </div>
          </Grid.Row>
        ) : (
          /**
           *  in cazul in care etajul contine camere alocate, camerele vor fi afisate ca lista cu numele(text), tipul(icon) camerei si numarul de locuri ocupate din totalul de locuri alocate
           *  fiecare camera va fi afisata de tip Accordion
           *  selectarea camerei va afisa:
           *  - numarul de locuri ocupate din totalul de locuri ale camerei
           *  - numarul de locuri alocat facultatii in camera respectiva
           *  - componenta Camera care afiseaza studentii care sunt cazati in camera respectiva
           */
          <Grid.Row centered className="gridStyle">
            <List className="listaCamere">
              {camere.map((camera, index) => {
                return (
                  <List.Item key={camera.ID_Camera}>
                    <Accordion styled fluid>
                      <Accordion.Title
                        active={activeIndex === camere.indexOf(camera)}
                        index={camere.indexOf(camera)}
                        onClick={this.handleClick}
                      >
                        {this.props.eSefAdminCamine ? (
                          <div>
                            <Icon name="zoom-in"/>
                            {camera.DenumireCamin}
                            Camera {camera.NumarCamera}
                            <Icon
                              className={
                                camera.TipCameraMF === "M"
                                  ? "blue male large icon"
                                  : camera.TipCameraMF === "F"
                                  ? "pink female large icon"
                                  : "black male large icon"
                              }
                            />
                            {camera.NrStudentiCazati}/{camera.NrLocuri}
                          </div>
                        ) : (
                          <div>
                            <Icon name="zoom-in"/>
                            Camera {camera.NumarCamera}
                            <Icon
                              className={
                                camera.TipCameraMF === "M"
                                  ? "blue male large icon"
                                  : camera.TipCameraMF === "F"
                                  ? "pink female large icon"
                                  : "black male large icon"
                              }
                            />
                            {camera.NrStudentiCazati}/
                            {camera.NrLocuriAlocateFacultate}
                          </div>
                        )}
                      </Accordion.Title>

                      <Accordion.Content
                        active={activeIndex === camere.indexOf(camera)}
                      >
                        <Grid>
                          <Grid.Row className="listaCamere" centered>
                            <Camera
                              listaCamine={this.props.listaCamine}
                              etapeCazare={this.props.etapeCazare}
                              idCaminSelectat={this.state.idCaminSelectat}
                              key={camera.ID_Camera}
                              index={index}
                              camera={camera}
                              anCazare={this.props.idAnCazare}
                              anUniversitarPrecazare={
                                this.props.anUniversitarPrecazare
                              }
                              returneazaStudent={this.props.returneazaStudent.bind(this)}
                              schimbaStatusStudent={this.schimbaStatusStudent.bind(this)}
                              scoateStudentDinCamera={this.scoateStudentDinCamera.bind(this)}
                              mutaStudent={this.mutaStudent.bind(this)}
                              nrLocuriTotal={this.state.nrLocuriTotal}
                              culoareFacultate={this.props.culoareFacultate}
                              idFacultate={this.props.idFacultate}
                              eSefAdminCamine={this.props.eSefAdminCamine}
                              studentiCamera={camera.StudentiCazati}
                              cazareDataDeCand={this.props.cazareDataDeCand}
                              cazareDataPanaCand={this.props.cazareDataPanaCand}
                              actualizeazaTipCameraMF={(
                                TipCameraMF,
                                ID_Camera
                              ) =>
                                this.actualizeazaTipCameraMF(
                                  TipCameraMF,
                                  ID_Camera
                                )
                              }
                              actualizeazaLabels={this.props.actualizeazaLabels.bind(
                                this
                              )}
                              selecteazaCamin={this.selecteazaCamin.bind(this)}
                              ID_TipCazare={this.props.ID_TipCazare}
                            />
                          </Grid.Row>
                        </Grid>
                      </Accordion.Content>
                    </Accordion>
                  </List.Item>
                );
              })}
            </List>
          </Grid.Row>
        )}
      </Grid>
    );
  }
}

export default ListaCamere;
