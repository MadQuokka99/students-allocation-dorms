import React from "react";
import axiosRef from "../../../axios-AGSIS-API-refactor";

import {Segment, Header, Modal, Button, Icon} from "semantic-ui-react";

import "react-semantic-toasts/styles/react-semantic-alert.css";

import Student from "../ListaStudenti/Student";
import {cazeazaStudent} from "../../../Utils/FunctiiCazare";
import {eStudentAdmitere} from "../../../Utils/StudentAdmitere";
import {toast} from "react-semantic-toasts";
import StudentProxy from "../ListaStudenti/StudentProxy";

class Camera extends React.PureComponent  {
  constructor(props) {
    super(props);
    this.state = {
      studentSelectatPtDecazare: [],
      modalOpenCazareConfirmare: false,
      modalOpenDecazeazaStudent: false,
      studentDecazatCazareConfirmata: false,
      randeazaProxyStudent: false
    };
  }



  /** actiunea de drop a studentilor in camere
   *  - apeleaza functia cazeazaStudent
   *  - apeleaza functia schimbaStatusStudent - pentru actualizarea listei de studenti din camera si a numarului de studenti cazati
   *  - apeleaza functia actualizeazaTipCameraMF - pentru actualizarea in interfata a genului camerei
   *  - apeleaza functia actualizeazaLabels - pentru actualizarea labelurilor cu numar studenti cazati, locuri libere fete/baieti/gen nespecificat
   *
   */
  drop = (event) => {
    event.preventDefault();

    if (event) {
      try {
        const data = event.dataTransfer.getData("transfer");
        let student = this.props.returneazaStudent(data.toString());
        //seteaza ID_Cazare cu un numar !== cu -1 pentru a se actualiza in interfata studentul (sa nu se mai afiseze ultima cazare si posibilitatea de recazare)
        //se va suprascrie corect dupa post
        student.ID_Cazare = 0;

        //randeaza un proxy pentru student pana cand se primeste response-ul de la server
        this.setState({randeazaProxyStudent: true})

        cazeazaStudent(
          student,
          this.props.camera.ID_Camera,
          this.props.cazareDataDeCand,
          this.props.cazareDataPanaCand,
          this.props.anCazare,
          this.props.idFacultate,
          this.props.ID_TipCazare
        )
          .then((r) => {

            //todo: schimbat mod actualizare
            //se actualizeaza studentul cu ID-ul de cazare primit
            student.ID_Cazare = r.ID_Cazare;
            //se actualizeaza studentul cu CazareExpirataLaDataCurenta=false
            student.CazareExpirataLaDataCurenta = false;
            student.ID_CameraUltimaCazare = r.ID_Camera;
            student.ID_TipCazare = this.props.ID_TipCazare;

            //pentru a face distinctia intre apelarea functiei din Student sau din Camera se trimite 1 pentru apelarea din Student si 0 din Camera
            this.props.schimbaStatusStudent(student, 0);
            this.props.actualizeazaTipCameraMF(r.TipCameraMF, r.ID_Camera);
            this.props.actualizeazaLabels();

          })
          .catch((e) => console.log(e))
          .finally(() => {
            this.setState({randeazaProxyStudent: false})
          })
      } catch (e) {
        console.log(e);
      }
    }
  };

  /**
   *
   * @param student
   *  metoda deschide modalul de confirmare decazare atunci cand este apasat butonul DecazeazaStudent
   */
  handleOpenDecazeazaStudent = (student) =>
    this.setState({
      studentSelectatPtDecazare: student,
      modalOpenDecazeazaStudent: true,
    });

  /**
   * metoda inchide modalul de decazare
   */
  handleCloseDecazeazaStudent = () =>
    this.setState({modalOpenDecazeazaStudent: false});

  /**
   * metoda deschide modalul de confirmare a decazarii in cazul in care studentul are cazarea confirmata (pentru cazul cand eSefAdminCamine=true)
   * sau modalul de atentionare ca studentul nu poate fi decazat deoarece are cazarea confirmata (in restul cazurilor)
   */
  handleOpenCazareConfirmata = () =>
    this.setState({
      modalOpenCazareConfirmare: true,
      studentDecazatCazareConfirmata: true,
    });

  /**
   * metoda inchide modalul ce se afiseaza atunci cand studentul are cazarea confirmata
   */
  handleCloseCazareConfirmata = () =>
    this.setState({
      modalOpenCazareConfirmare: false,
      modalOpenDecazeazaStudent: false,
    });

  /**
   * metoda este apelata atunci cand este apasat butonul "Da" din modalul de Decazeaza student si
   * apeleaza la randul sau functia de decazare
   */
  handleClickDecazeaza = () => {
    this.decazeazaStudent(
      this.state.studentSelectatPtDecazare,
      this.props.camera
    );
    this.handleCloseDecazeazaStudent();
  };

  /**
   * metoda este apelata atunci cand studentul are cazarea confirmata, dar eSefAdminCamine=true si apare cel de-al doilea modal si este apasat butonul "Da"
   * se apeleaza, de asemenea, functia de decazare
   */
  handleClickDecazeazaCazareConfirmata = () => {
    this.decazeazaStudent(
      this.state.studentSelectatPtDecazare,
      this.props.camera
    );
    this.handleCloseCazareConfirmata();
  };

  /**
   *
   * @param student
   * @param camera
   *
   * este apelat end-point-ul de decazare daca:
   *  - eSefAdminCamine = true sau
   *  - studentul nu are cazarea confirmata sau
   *  - eSefAdminCamine=false si studentul nu are cazarea confirmata
   * in caz contrar, se apeleaza functia handleOpenCazareConfirmata, care retine prin parametrul studentDecazatCazareConfirmata daca persoana care face decazarea are acest drept sau nu
   *
   * apeleaza functia scoateStudentDinCamera - pentru stergerea studentului decazat din lista de studenti din camera
   * apeleaza functia actualizeazaTipCameraMF - pentru actualizarea in interfata a genului camerei
   * apeleaza functia actualizeazaLabels - pentru actualizarea labelurilor cu numar studenti cazati, locuri libere fete/baieti/gen nespecificat
   */
  decazeazaStudent(student, camera) {
    //console.log(student);
    if (this.state.studentDecazatCazareConfirmata) {
      axiosRef
        .post(
          "Cazare/CazareDecazeazaStudentCuStergereTaxe?ID_Cazare=" +
          student.ID_Cazare
        )
        .then((response) => {
          //console.log(response);

          /**
           * toast de informare decazare reusita
           */
          toast({
            type: "success",
            icon: "check",
            title: "Decazarea a fost făcută cu succes!",
            time: 4000,
          });

          try {
            this.props.scoateStudentDinCamera(student, camera, response.data);
            this.props.actualizeazaTipCameraMF(
              response.data.TipCameraMF,
              response.data.ID_Camera
            );
            this.props.actualizeazaLabels();
            this.handleCloseDecazeazaStudent();
            this.setState({
              studentDecazatCazareConfirmata: false,
            });
          } catch (e) {
            console.log(e);
          }
        })
        .catch((error) => {
          alert(error.response.data);
        });
    } else if (
      (!this.props.eSefAdminCamine && !student.CazareConfirmata) ||
      (this.props.eSefAdminCamine && !student.CazareConfirmata)
    ) {
      axiosRef
        .post(
          "Cazare/CazareDeleteFaraStergereTaxe?ID_Cazare=" + student.ID_Cazare
        )
        .then((response) => {
          //console.log(response);

          /**
           * toast de informare decazare reusita
           */
          toast({
            type: "success",
            icon: "check",
            title: "Decazarea a fost făcută cu succes!",
            time: 4000,
          });

          try {
            this.props.scoateStudentDinCamera(student, camera, response.data);
            this.props.actualizeazaTipCameraMF(
              response.data.TipCameraMF,
              response.data.ID_Camera
            );
            this.props.actualizeazaLabels();
            this.handleCloseDecazeazaStudent();
          } catch (e) {
            console.log(e);
          }
        })
        .catch((error) => {
          alert(error.response.data);
        });
    } else if (student.CazareConfirmata) {
      this.handleOpenCazareConfirmata();
    }
  }

  /**
   * metoda asigura apelarea metodei drop
   */
  allowDrop = (event) => {
    event.preventDefault();
  };

  /**
   * este returnata o lista a studentilor cazati prin apelarea componentei Student
   * permite actiunea de drag&drop a studentului in camera
   */
  render() {
    //console.log(this.state.studenti);

    return (
      /**
       * camera permite actiunea de drop asupra componentei Camera doar cand nr de studenti cazati este mai mic decat nr de locuri alocate facultatii
       */
      <div
        onDrop={
          (this.props.camera.NrStudentiCazati <
            this.props.camera.NrLocuriAlocateFacultate )
            ? this.drop
            : undefined
        }
        onDragOver={
          (this.props.camera.NrStudentiCazati <
            this.props.camera.NrLocuriAlocateFacultate )
            ? this.allowDrop
            : undefined
        }
        className="listaCamere"
      >
        {this.props.studentiCamera.map((student, index) => {
          return (
            <div key={index}>
              <Student
                idAnCazare={this.props.anCazare}
                idFacultate={this.props.idFacultate}
                culoareFacultate={this.props.culoareFacultate}
                student={student}
                key={student.ID_Student}
                listaCamine={this.props.listaCamine}
                eSefAdminCamine={this.props.eSefAdminCamine}
                handleOpenDecazeazaStudent={this.handleOpenDecazeazaStudent.bind(this)}
                schimbaStatusStudent={this.props.schimbaStatusStudent.bind(this)}
                actualizeazaTipCameraMF={this.props.actualizeazaTipCameraMF.bind(this)}
                actualizeazaLabels={this.props.actualizeazaLabels.bind(this)}
                eStudentAdmitere={eStudentAdmitere(
                  student,
                  this.props.anCazare
                )}
                mutaStudent={this.props.mutaStudent}
                tipRandareStudent={"camera"}
              />
            </div>
          );
        })}

        {
          this.state.randeazaProxyStudent &&
          <StudentProxy/>
        }

        <br/>
        <br/>

        {
          //segmentul "Insereaza student" apare doar cand nr de studenti este mai mic decat nr de locuri in camera SAU cand se face o cazare de vara
          (this.props.camera.NrStudentiCazati <
            this.props.camera.NrLocuriAlocateFacultate ) && (
            <Segment className="insereazaStudentSegment">
              {"Inserează studentul"}
            </Segment>
          )
        }

        <Modal open={this.state.modalOpen} onClose={this.handleClose}>
          <Header icon="attention"/>
          <Modal.Content>
            <h3>{this.state.mesaj}</h3>
          </Modal.Content>

          <Modal.Actions>
            <Button
              type="button"
              color="green"
              onClick={this.handleClose}
              inverted
            >
              <Icon name="checkmark"/> Ok
            </Button>
          </Modal.Actions>
        </Modal>

        {/*pentru actiunea de decazare student din camera*/}
        <Modal
          closeIcon
          open={this.state.modalOpenDecazeazaStudent}
          onClose={this.handleCloseDecazeazaStudent}
        >
          <Header
            icon="arrow alternate circle left outline"
            content="Decazare"
          />
          <Modal.Content>
            <p>
              Doresti sa decazezi studentul{" "}
              {this.state.studentSelectatPtDecazare.NumeIntreg}?
            </p>
          </Modal.Content>
          <Modal.Actions>
            <Button
              basic
              color="red"
              onClick={this.handleCloseDecazeazaStudent}
            >
              <Icon name="remove"/> Nu
            </Button>
            <Button color="green" inverted onClick={this.handleClickDecazeaza}>
              <Icon name="checkmark"/> DA
            </Button>
          </Modal.Actions>
        </Modal>

        {/*pentru actiunea de decazare a studentului care are cazarea confirmata, actiune care poate fi facuta doar cand eSefAdmineCamin=true*/}

        {this.props.eSefAdminCamine ? (
          <Modal
            closeIcon
            open={this.state.modalOpenCazareConfirmare}
            onClose={this.handleCloseCazareConfirmata}
          >
            <Header
              icon="arrow alternate circle left outline"
              content="Decazare"
            />
            <Modal.Content>
              <p>
                Studentul {this.state.studentSelectatPtDecazare.NumeIntreg} a
                confirmat deja, doriți să îl decazați?
              </p>
            </Modal.Content>
            <Modal.Actions>
              <Button
                basic
                color="red"
                onClick={this.handleCloseCazareConfirmata}
              >
                <Icon name="remove"/> Nu
              </Button>
              <Button
                color="green"
                inverted
                onClick={this.handleClickDecazeazaCazareConfirmata}
              >
                <Icon name="checkmark"/> DA
              </Button>
            </Modal.Actions>
          </Modal>
        ) : (
          /*pentru actiunea de informare a utilizatorului de faptul ca studentul nu poate fi sters deoarece are cazarea confirmata*/
          <Modal
            closeIcon
            open={this.state.modalOpenCazareConfirmare}
            onClose={this.handleCloseCazareConfirmata}
          >
            <Header
              icon="arrow alternate circle left outline"
              content="Decazare"
            />
            <Modal.Content>
              <p>
                Studentul {this.state.studentSelectatPtDecazare.NumeIntreg} a
                confirmat deja, nu poate fi șters!
              </p>
            </Modal.Content>
            <Modal.Actions>
              <Button
                basic
                color="blue"
                onClick={this.handleCloseCazareConfirmata}
              >
                Ok
              </Button>
            </Modal.Actions>
          </Modal>
        )}
      </div>
    );
  }
}

export default Camera;
