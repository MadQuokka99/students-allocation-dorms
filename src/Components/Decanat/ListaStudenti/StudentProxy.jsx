import {
  Label,
  Placeholder,
  Table
} from "semantic-ui-react";
import React from "react";

import "./Student.css"

export default function StudentProxy() {
  return <div
    className="studentStyle"
  >
    <Table className="cellTabelStyle">
      <Table.Body className="shadow">
        <Table.Row>
          <Table.Cell collapsing>
            <Label
              className="genderLabel"
            >
              -
            </Label>
          </Table.Cell>

          <Table.Cell className="numeStudent">
            <Placeholder>
              <Placeholder.Header>
                <Placeholder.Line length='medium'/>
              </Placeholder.Header>
            </Placeholder>

          </Table.Cell>


        </Table.Row>

        <Table.Row>
          <Table.Cell colSpan="4">
            <Placeholder>
              <Placeholder.Paragraph>
                <Placeholder.Line length='medium'/>
                <Placeholder.Line length='short'/>
              </Placeholder.Paragraph>
            </Placeholder>
          </Table.Cell>
        </Table.Row>
      </Table.Body>
    </Table>
  </div>
}