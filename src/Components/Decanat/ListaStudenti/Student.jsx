import React, {Component} from "react";
import {
  Table,
  Label,
  Icon,
  Popup,
  Comment,
  Header,
  Button,
  Modal,
  Dropdown
} from "semantic-ui-react";
import "./Student.css";

import moment from "moment";

import {cazeazaStudent} from "../../../Utils/FunctiiCazare";
// import InterschimbaStudent from "../../InterschimbaStudent";
import MutaStudent from "../../MutaStudent";
import IstoricComentarii from "../IstoricComentarii";

const denumireSpecializareLenghtLimit = 30;

/**
 * culoarea pentru labelul de ciclu invatament este luata in functie de ID_TipCiclu
 *
 */
let culoareID_TipCiclu = {
  2: "#5e83ba", //licenta
  4: "#475989", //master
  5: "#013c6f", //doctorat
  7: "#83c1e8", //studii postuniversitare
  8: "#83c1e8", //an pregatitor
};

class Student extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openModalRepetaCazare: false,
    };
  }

  drag = (event) => {
    const target = event.target;

    event.dataTransfer.setData("transfer", this.props.student.ID_Student);

    setTimeout(() => {
      target.style.display = "none";
    }, 0);
  };

  dragOver = (event) => {
    event.stopPropagation();
  };

  /**
   * daca studentul a fost cazat si nu si-a confirmat cazarea si e decazat, ii apare bara portocalie
   * daca are cerere de precazare si nu a fost cazat, ii apare bara verde
   */
  getStudentStatus = () => {
    //daca e selectata optiunea pentru afiseaza doar cei cu cerere
    //se vor colora portocaliu cei decazati
    if (
      this.props.afiseazaDoarCeiCuCerere &&
      this.props.student.NuAConfirmatLaCazare
    )
      return "orange";

    //daca nu e selectata optiunea se vor colara cu portocaliu cei decazati
    //iar apoi cu verde cei cu cerere
    if (this.props.afiseazaDoarCeiCuCerere === false) {
      if (this.props.student.NuAConfirmatLaCazare) return "orange";
      if (this.props.student.ID_CererePrecazare !== -1) return "green";
    }

    //daca studentul e deja intr-o camera, dar nu a confirmat, se va colora portocaliu
    if (
      !this.props.student.CazareConfirmata &&
      this.props.student.ID_Cazare !== -1
    )
      return "orange";
  };

  eNeimportatInAnulUrmator = () => {
    //daca anul universitar e diferit de anCazare, studentul nu a fost importat pe anul curent (nu i s-a actualizat
    //campul de an studiu)
    if (
      this.props.student.ID_AnUniv !== this.props.idAnCazare &&
      !this.props.eStudentAdmitere
    )
      return true;
    return false;
  };

  handleModalRepetaCazare = (closeOnEscape, closeOnDimmerClick) => () => {
    this.setState({
      closeOnEscape,
      closeOnDimmerClick,
      openModalRepetaCazare: true,
    });
  };

  closeModalRecazare = () => {
    this.setState({openModalRepetaCazare: false});
  };

  /**
   * metoda cazeaza studentul ales prin apasarea butonului "Repeta cazarea anterioara"
   *  - apeleaza functia actualizeazaTipCameraMF - pentru actualizarea in interfata a genului camerei
   *  - apeleaza functia actualizeazaLabels - pentru actualizarea labelurilor cu numar studenti cazati, locuri libere fete/baieti/gen nespecificat
   */
  efectueazaCazare = () => {
    this.closeModalRecazare();

    //console.log(this.props.student);

    cazeazaStudent(
      this.props.student,
      this.props.student.ID_CameraUltimaCazare,
      this.props.cazareDataDeCand,
      this.props.cazareDataPanaCand,
      this.props.anCazare.ID_AnUniv,
      this.props.idFacultate,
      this.props.ID_TipCazare
    )
      .then((r) => {
        try {
          //se actualizeaza studentul cu ID-ul de cazare primit
          this.props.student.ID_Cazare = r.ID_Cazare;
          //se actualizeaza studentul cu CazareExpirataLaDataCurenta=false
          this.props.student.CazareExpirataLaDataCurenta = false;

          this.props.actualizeazaLabels();

          //pentru a face distinctia intre apelarea functiei din Student sau din Camera se trimite 1 pentru apelarea din Student si 0 din Camera
          this.props.schimbaStatusStudent(this.props.student, 1);
          this.props.actualizeazaTipCameraMF(r.TipCameraMF, r.ID_Camera);
        } catch (e) {
          console.log(e);
        }
      })
      .catch((e) => console.log(e));
  };

  /**
   * metoda apeleaza functia handleOpenDecazeazaStudent cu parametrul student din componenta Camera pentru a caza studentul trimis
   */
  handleClickDecazeaza = () => {
    //console.log(this.props.student);
    this.props.handleOpenDecazeazaStudent(this.props.student);
  };

  render() {
    return (
      <div
        id={this.props.student.ID_Student}
        //proprietatea draggable a studentului devine true daca studentul are CazareExpirataLaDataCurenta=null si false daca CazareExpirataLaDataCurenta are orice alta valoare
        //acest lucru impiedica posibilitatea utilizatorului de a putea face drag&drop cu studentul cazat in camera (in partea dreapta a paginii)
        // si lasa aceasta proprietate doar atunci cand studentul se afla in lista de studenti care au primit cazare (in partea stanga a paginii)
        draggable={
          this.props.student.CazareExpirataLaDataCurenta === null
            ? true
            : this.props.student.CazareExpirataLaDataCurenta
        }
        onDragStart={this.drag}
        onDragOver={this.dragOver}
        onDragEnd={() => {
          this.props.actualizeazaListaStudenti();
        }}
        className="studentStyle"
      >
        <Table className="cellTabelStyle" color={this.getStudentStatus()}>
          <Table.Body className="shadow">
            <Table.Row>
              <Table.Cell collapsing>
                {/*sexul studentului; se coloreaza in functe de F/M*/}
                <Label
                  className="genderLabel"
                  color={this.props.student.Sex === "M" ? "blue" : "pink"}
                >
                  {this.props.student.Sex}
                </Label>
              </Table.Cell>

              <Table.Cell className="numeStudent">
                {this.props.student.CazareConfirmata && (
                  <Popup
                    hoverable
                    mouseLeaveDelay={500}
                    hideOnScroll
                    content="Studentul are cazarea confirmată"
                    trigger={
                      <Icon
                        name="check"
                        style={{
                          color: "green",
                        }}
                      />
                    }
                  />
                )}

                {
                  (this.props.student.ID_TipCazare === 2) &&
                  <Popup
                    hoverable
                    mouseLeaveDelay={500}
                    hideOnScroll
                    content={"Cazare de vara"}
                    trigger={
                      <Icon
                        name="sun"
                        color="yellow"
                      />
                    }
                  />
                }

                {this.props.student.NumeIntreg}
                <Popup
                  hoverable
                  mouseLeaveDelay={500}
                  hideOnScroll
                  content={
                    this.props.student.Telefon
                      ? this.props.student.Telefon
                      : "-"
                  }
                  trigger={
                    <Icon
                      className="phone"
                      style={{
                        color: this.props.culoareFacultate,
                        marginLeft: "10px",
                      }}
                    />
                  }
                />
                <Popup
                  wide
                  hoverable
                  mouseLeaveDelay={500}
                  hideOnScroll
                  content={
                    <div>
                      <Icon
                        className="student"
                        style={{
                          color: this.props.culoareFacultate,
                        }}
                      />
                      {this.props.student.Username
                        ? this.props.student.Username
                        : "-"}
                      <br/>
                      <Icon
                        className="mail square"
                        style={{
                          color: this.props.culoareFacultate,
                        }}
                      />
                      {this.props.student.Email
                        ? this.props.student.Email
                        : "-"}
                    </div>
                  }
                  trigger={
                    <Icon
                      className="mail"
                      style={{
                        color: this.props.culoareFacultate,
                        marginLeft: "10px",
                      }}
                    />
                  }
                />
                {this.props.student.EsteRepetent &&
                <Popup
                  hoverable
                  mouseLeaveDelay={500}
                  hideOnScroll
                  content={"Studentul este repetent!"}
                  trigger={
                    <Icon
                      className="exclamation triangle"
                      color={"red"}
                      style={{
                        marginLeft: "10px",
                      }}
                    />
                  }
                />
                }
                {(this.props.student.LichidatCamine === false || this.props.student.ComentariiCamine?.length > 0) &&
                <Popup
                  flowing
                  hoverable
                  content={
                    <Comment.Group>
                      <Header as="h3" dividing>
                        Istoric Comentarii
                      </Header>
                      <IstoricComentarii
                        listaComentarii={
                          this.props.student.ComentariiCamine
                        }
                      />
                    </Comment.Group>
                  }
                  position="bottom center"
                  trigger={
                    <Label
                    >
                      <Icon
                        color="yellow"
                        className="warning circle"
                      />
                      Lichidare
                    </Label>
                  }
                />
                }
              </Table.Cell>

              {/*randare conditionata pentru cerere mutare*/}
              <Table.Cell>
                {this.props.student.DoresteMutare && (
                  <div>
                    <Popup
                      position="bottom center"
                      hoverable
                      mouseLeaveDelay={500}
                      hideOnScroll
                      basic
                      content={
                        <div>
                          <Header as="h4" dividing>
                            Dorește mutare
                          </Header>
                          <Comment>
                            <Comment.Content>
                              <Comment.Author>
                                <strong>Motiv cerere:</strong>
                              </Comment.Author>
                              <Comment.Text>
                                {this.props.student.MotivCerereMutare}
                              </Comment.Text>
                            </Comment.Content>
                          </Comment>
                          <Comment>
                            <Comment.Content>
                              <Comment.Author>
                                <strong>Dorește în:</strong>
                              </Comment.Author>
                              <Comment.Text>
                                <Label
                                  className="alertLabel"
                                  style={{
                                    backgroundColor: this.props
                                      .culoareFacultate,
                                  }}
                                >
                                  {this.props.student.DoresteInDenumireCamin}
                                </Label>
                                <Label
                                  className="alertLabel"
                                  style={{
                                    backgroundColor: this.props
                                      .culoareFacultate,
                                  }}
                                >
                                  Camera
                                  <Label.Detail>
                                    {this.props.student.DoresteInNumarCamera}
                                  </Label.Detail>
                                </Label>
                              </Comment.Text>
                            </Comment.Content>
                          </Comment>
                        </div>
                      }
                      trigger={
                        <Icon className="exclamation triangle" color="orange"/>
                      }
                    />
                  </div>
                )}
              </Table.Cell>
              {/* randare conditionata pentru istoricul de cazare*/}
              <Table.Cell textAlign={"right"}>
                {(this.props.student.ID_Cazare === -1 ||
                  this.props.student.ID_Cazare === 0) && (
                  <span>

                    {this.props.student.ID_CazareVara !== null &&
                    <Popup
                      hoverable
                      mouseLeaveDelay={500}
                      hideOnScroll
                      position="bottom right"
                      content={<div>
                        <strong>A fost cazat pe vara</strong>
                        <table>
                          <tbody>
                          <tr>
                            <td>
                              Camin:
                            </td>
                            <td>
                              <strong>{this.props.student.CazatInDenumireCaminCazareVara}</strong>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              Data pana cand:
                            </td>
                            <td>
                              <strong> {moment(this.props.student.CazatDataPanaCandCazareVara).format("DD.MM.YYYY")}</strong>
                            </td>
                          </tr>
                          </tbody>
                        </table>
                      </div>
                      }
                      trigger={
                        <Icon
                          name="sun"
                          color="yellow"
                          size={"large"}
                          disabled={this.props.student.CazareVaraExpirata}
                        />
                      }
                    />
                    }

                    {this.props.student.ID_UltimaCazare !== -1 && (
                      <span>
                        <Label
                          className="alertLabel"
                          style={{
                            backgroundColor: this.props.culoareFacultate,
                          }}
                        >
                          {this.props.student.DenumireCaminUltimaCazare}
                        </Label>
                        <Label
                          className="alertLabel"
                          style={{
                            backgroundColor: this.props.culoareFacultate,
                          }}
                        >
                          Camera
                          <Label.Detail>
                            {this.props.student.NumarCameraUltimaCazare}
                          </Label.Detail>
                        </Label>
                        <Popup
                          hoverable
                          mouseLeaveDelay={500}
                          hideOnScroll
                          position="bottom right"
                          content={<div>
                            {
                              this.props.student.ID_AnUnivUltimaCazare === this.props.idAnCazare &&
                              <strong style={{color: "red"}}>A fost cazat deja anul acesta!</strong>
                            }
                            <table>
                              <tbody>
                              <tr>
                                <td>
                                  Data cazare:
                                </td>
                                <td>
                                  <strong>{moment(this.props.student.DataDeCandUltimaCazare).format("DD.MM.YYYY")}</strong>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  Data decazare:
                                </td>
                                <td>
                                  <strong> {moment(this.props.student.DataPanaCandUltimaCazare).format("DD.MM.YYYY")}</strong>
                                </td>
                              </tr>
                              </tbody>
                            </table>
                          </div>
                          }
                          trigger={
                            <Button
                              style={{
                                backgroundColor: this.props.student.ID_AnUnivUltimaCazare === this.props.idAnCazare ? "red" : this.props.culoareFacultate,
                                color: "#ffffff"
                              }}
                              icon="calendar alternate"
                              type="button"
                              size="mini"
                            />
                          }
                        />

                        <Popup
                          hoverable
                          mouseLeaveDelay={500}
                          hideOnScroll
                          position="bottom right"
                          content={"Repetă cazarea anterioară"}
                          trigger={
                            <Button
                              color="green"
                              icon="redo"
                              type="button"
                              size="mini"
                              onClick={this.handleModalRepetaCazare(true, true)}
                            />
                          }
                        />
                        <Modal
                          size={"small"}
                          open={this.state.openModalRepetaCazare}
                          onClose={this.closeModalRecazare}
                        >
                          <Modal.Header>REPETĂ CAZAREA ANTERIOARĂ</Modal.Header>

                          <Modal.Actions>
                            <Button
                              basic
                              onClick={this.closeModalRecazare}
                              negative
                              type="button"
                            >
                              RENUNȚĂ
                            </Button>
                            <Button
                              onClick={this.efectueazaCazare}
                              type="button"
                              basic
                              positive
                              labelPosition="right"
                              icon="checkmark"
                              content="DA"
                            />
                          </Modal.Actions>
                        </Modal>
                      </span>
                    )}
                  </span>
                )}
                {(this.props.student.CazareExpirataLaDataCurenta === false && this.props.tipRandareStudent==="camera")  && (
                  /**
                   * dropdown menu care permite fie mutarea unui student, fie interschimbarea sa cu un alt student cazat
                   */
                  <span>
                    {
                      this.props.eSefAdminCamine && <Dropdown item direction="left" icon="setting" style={{
                        paddingRight: "7px",
                      }}>
                        <Dropdown.Menu>
                          <MutaStudent
                            student={this.props.student}
                            listaCamine={this.props.listaCamine}
                            idFacultate={this.props.idFacultate}
                            eSefAdminCamine={this.props.eSefAdminCamine}
                            idAnCazare={this.props.idAnCazare}
                            mutaStudent={this.props.mutaStudent}
                          />
                          {/*<InterschimbaStudent*/}
                          {/*    student={this.props.student}*/}
                          {/*    idFacultate={this.props.idFacultate}*/}
                          {/*    eSefAdminCamine={this.props.eSefAdminCamine}*/}
                          {/*    idAnCazare={this.props.idAnCazare}*/}
                          {/*/>*/}
                        </Dropdown.Menu>
                      </Dropdown>
                    }
                    {
                      // (this.props.eSefAdminCamine || this.props.student.CazareConfirmata === false) &&
                       (this.props.eSefAdminCamine ) &&
                      <Button
                        icon="x"
                        type="button"
                        size="mini"
                        color="red"
                        onClick={this.handleClickDecazeaza}
                      />
                    }

                  </span>
                )}
              </Table.Cell>
            </Table.Row>

            <Table.Row>
              <Table.Cell colSpan="4">
                {/*Judetul de provenienta*/}
                <Label className="infoLabel">
                  JUD
                  <Label.Detail>
                    {this.props.student.DenumireJudet}
                  </Label.Detail>
                </Label>


                {/*Localitatea de provenienta*/}
                <Label className="infoLabel">
                  LOC
                  <Label.Detail>
                    {this.props.student.DenumireLocalitate}
                  </Label.Detail>
                </Label>
                <Label className="infoLabel">
                  {this.props.student?.DenumireFormaFinantareInterna}
                </Label>
                {/*Afiseaza conditionat situatia sociala speciala*/}
                {this.props.student.Stare_sociala_speciala !== "Nu e cazul" &&
                this.props.student.Stare_sociala_speciala !==
                "Nedeclarat" && (
                  <Label
                    className="alertLabel"
                    style={{
                      backgroundColor: this.props.culoareFacultate,
                    }}
                  >
                    {this.props.student.Stare_sociala_speciala}
                  </Label>
                )}

                {/*Afiseaza conditionat daca e olimpic*/}
                {this.props.student.CandidatOlimpic === true && (
                  <Label
                    className="alertLabel"
                    style={{
                      backgroundColor: this.props.culoareFacultate,
                    }}
                  >
                    Olimpic
                  </Label>
                )}

                {/*Afiseaza conditionat situatia medicala speciala*/}
                {this.props.student.Situatie_medicala_speciala !==
                "Nu e cazul" &&
                this.props.student.Situatie_medicala_speciala !==
                "Nedeclarat" && (
                  <Label
                    className="alertLabel"
                    style={{
                      backgroundColor: this.props.culoareFacultate,
                    }}
                  >
                    {this.props.student.Situatie_medicala_speciala}
                  </Label>
                )}
                <br/>

                {/*Licenta/Master/Doctorat*/}
                <Label
                  className="alertLabel"
                  style={{
                    backgroundColor:
                      culoareID_TipCiclu[this.props.student.ID_TipCiclu],
                  }}
                >
                  {this.props.student.DenumireCicluInv}
                </Label>

                {/*An de studiu*/}
                <Label
                  className={
                    this.props.eStudentAdmitere ? "alertLabel" : "infoLabel"
                  }
                  style={
                    this.props.eStudentAdmitere
                      ? {backgroundColor: "orange"}
                      : {}
                  }
                  title={
                    this.props.eStudentAdmitere ? "Student din admitere" : ""
                  }
                >
                  AN
                  <Label.Detail>
                    {this.props.student.DenumireAnStudiu}
                    {this.eNeimportatInAnulUrmator() ? (
                      <Popup
                        content="Studentul nu a fost importat în anul curent de studiu."
                        trigger={
                          <Icon
                            color="yellow"
                            className="warning circle"
                            style={{marginLeft: "5px"}}
                          />
                        }
                      />
                    ) : null}
                  </Label.Detail>
                </Label>

                {/*Specializarea*/}
                <Popup
                  position="bottom center"
                  hoverable
                  mouseLeaveDelay={500}
                  hideOnScroll
                  content={this.props.student.DenumireSpecializare}
                  trigger={
                    <Label className="infoLabel">
                      SPECIALIZARE
                      <Label.Detail>
                        {this.props.student.DenumireScurtaSpecializare
                          ? this.props.student.DenumireScurtaSpecializare
                          : this.props.student.DenumireSpecializare.substr(
                            0,
                            denumireSpecializareLenghtLimit
                          ) +
                          (this.props.student.DenumireSpecializare.length >
                          denumireSpecializareLenghtLimit
                            ? " ..."
                            : "")}
                      </Label.Detail>
                    </Label>
                  }
                />

                {/*Grupa*/}
                <Label className="infoLabel">
                  {this.props.student.DenumireGrupa}
                </Label>
                <br/>
                <div style={{float: "left"}}>
                  <Label title={"Mediile de dinainte de sesiunea de reexaminari"} basic className="alertLabel"
                         color="grey">
                    - RR:{"   "}
                  </Label>
                  {/*Media de admitere*/}
                  {this.props.eStudentAdmitere ? (
                    <Label className="infoLabel">
                      MEDIA ADMITERE
                      <Label.Detail>
                        {this.props.student.MedieAdmitere}
                      </Label.Detail>
                    </Label>
                  ) : this.props.student.CazareNrExameneCreditate === 0 ? (
                    <Label className="infoLabel">
                      MEDIA
                      <Label.Detail>
                        {this.props.student.CazareMedieAnCuFacultative}
                      </Label.Detail>
                    </Label>
                  ) : (
                    this.props.eStudentAdmitere === false && (
                      <Label className="infoLabel">
                        RESTANȚE
                        <Label.Detail>
                          {this.props.student.CazareNrExameneCreditate}
                        </Label.Detail>
                      </Label>
                    )
                  )}

                  {this.props.eStudentAdmitere === false &&
                  this.props.student.CazareNrExameneCreditate === 0 && (
                    <Popup
                      position="bottom center"
                      hoverable
                      mouseLeaveDelay={500}
                      hideOnScroll
                      content="Punctaj Valoric"
                      trigger={
                        <Label className="infoLabel">
                          PUNCTAJ
                          <Label.Detail>
                            {this.props.student.CazareMediaPunctajValoric}
                          </Label.Detail>
                        </Label>
                      }
                    />
                  )}
                </div>
                <div style={{float: "right"}}>
                  <Label basic className="alertLabel" color="grey">
                    ACUM:{"   "}
                  </Label>
                  {/*Media de admitere*/}
                  {this.props.eStudentAdmitere ? (
                    <Label className="infoLabel">
                      MEDIA ADMITERE
                      <Label.Detail>
                        {this.props.student.MedieAdmitere}
                      </Label.Detail>
                    </Label>
                  ) : this.props.student.NrExameneCreditate === 0 ? (
                    <Label className="infoLabel">
                      MEDIA
                      <Label.Detail>
                        {this.props.student.MediaAnCuFacultative}
                      </Label.Detail>
                    </Label>
                  ) : (
                    this.props.eStudentAdmitere === false && (
                      <Label className="infoLabel">
                        RESTANȚE
                        <Label.Detail>
                          {this.props.student.NrExameneCreditate}
                        </Label.Detail>
                      </Label>
                    )
                  )}

                  {this.props.eStudentAdmitere === false &&
                  this.props.student.NrExameneCreditate === 0 && (
                    <Popup
                      position="bottom center"
                      hoverable
                      mouseLeaveDelay={500}
                      hideOnScroll
                      content="Punctaj Valoric"
                      trigger={
                        <Label className="infoLabel">
                          PUNCTAJ
                          <Label.Detail>
                            {this.props.student.PunctajDacaIntegralist_An}
                          </Label.Detail>
                        </Label>
                      }
                    />
                  )}
                </div>
              </Table.Cell>
            </Table.Row>
          </Table.Body>
        </Table>
      </div>
    );
  }
}

export default Student;
