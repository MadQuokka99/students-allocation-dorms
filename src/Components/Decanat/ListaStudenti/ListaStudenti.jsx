import React, {Component, createRef} from "react";
import "./ListaStudenti.css";
import Student from "./Student";
import axiosRef from "../../../axios-AGSIS-API-refactor";
import {groupBy} from "../../../Utils/GroupBy";
import {eStudentAdmitere} from "../../../Utils/StudentAdmitere";
import {
  List,
  AutoSizer,
  CellMeasurer,
  CellMeasurerCache,
} from "react-virtualized";
import {
  Grid,
  Form,
  Button,
  Icon,
  Popup,
  Dropdown,
  TextArea,
  Message,
} from "semantic-ui-react";
import LabelStatistici from "../../LabelStatistici";
import {functieCautareTextStr} from "../../../Utils/FunctiiText";

const cache = new CellMeasurerCache({
  fixedWidth: true,
  defaultHeight: 50,
});

const droppableStyle = {
  justifyContent: "center",
  color: "white",
  width: "75%",
  height: "700px",
};

class ListaStudenti extends Component {
  contextRef = createRef();

  constructor(props) {
    super(props);
    this.state = {
      cautare: "",

      afiseazaDoarCeiCuCerere: true,

      afiseazaFields: {
        afiseazaStudentiAdmitere: false,
        afiseazaStudentiCazareVara: false,
        afiseazaFete: false,
        afiseazaBaieti: false,
        afiseazaLicenta: false,
        afiseazaMaster: false,
        afiseazaOlimpici: false,
        afiseazaOrfani: false,
        afiseazaCasaDeCopii: false,
        afiseazaProblemeMedicale: false,
        afiseazaNrRestante2: false,
      },

      ordoneazaFields: {
        ordoneazaRestante: false,
        ordoneazaMedie: false,
        ordoneazaPunctaj: false,
        ordoneazaCazareRestante: false,
        ordoneazaCazareMedie: false,
        ordoneazaCazarePunctaj: false,
        ordoneazaMedieAdmitere: false,
        ordoneazaAlfabeticAZ: false,
        ordoneazaAlfabeticZA: false,
        ordineAnStudiuAscendent: false,
        ordineAnStudiuDescendent: false,
      },

      grupeazaFields: {
        grupeazaDupaLocalitate: false,
        grupeazaDupaUltimaCazare: false,
        grupeazaDupaGrupa: false,
      },

      specializari: [],
      specializareSelectata: null,

      aniStudiu: [],
      anStudiuSelectat: null,

      formeFinantare: [],
      formaFinantareSelectata: null,

      keyVirtualizedList: 1,
      virtualizedListIndexToScrollTo: undefined,
    };
  }

  componentDidMount() {
    axiosRef
      .get(
        `Facultate/SpecializareListByFacultateAnUniv?iD_Facultate=${this.props.idFacultate}&iD_AnUniv=${this.props.anCurent.ID_AnUniv}`
      )
      .then((specializari) => {
        let listaSpecializari = [];
        let specializareData = {text: "TOATE SPECIALIZĂRILE", value: -1};
        listaSpecializari.push(specializareData);
        specializari.data.forEach((specializare) => {
          let specializareData = {
            text: specializare.DenumireSpecializare,
            value: specializare.ID_Specializare,
          };
          if (
            !specializareData.text
              .substring(
                specializareData.text.indexOf("..."),
                specializareData.text.length
              )
              .normalize("NFD")
              .toLowerCase()
              .replace(/[\u0300-\u036f]/g, "")
              .replace(/\s/g, "")
              .includes("distanta")
          )
            listaSpecializari.push(specializareData);
          this.setState({specializari: listaSpecializari});
        });
      });
    axiosRef
      .get(
        `Facultate/AnStudiuListByFacultate?ID_Facultate=${this.props.idFacultate}`
      )
      .then((aniStudiu) => {
        let listaAniStudiu = [];
        let anStudiuData = {text: "TOȚI ANII DE STUDIU", value: -1};
        listaAniStudiu.push(anStudiuData);
        aniStudiu.data.forEach((anStudiu) => {
          let anStudiuData = {
            text: "Anul " + anStudiu.Denumire,
            value: anStudiu.ID_AnStudiu,
          };
          listaAniStudiu.push(anStudiuData);
          this.setState({aniStudiu: listaAniStudiu});
        });
      });
    axiosRef.get(`Cazare/FormaFinantareListDenumiri`).then((formeFinantare) => {
      let listaFormeFinantare = [];
      let formaFinantareData = {
        text: "TOATE FORMELE DE FINANȚARE",
        value: -1,
      };
      listaFormeFinantare.push(formaFinantareData);
      formeFinantare.data.forEach((formaFinantare, index) => {
        let formaFinantareData = {
          text: formaFinantare,
          value: index,
        };
        listaFormeFinantare.push(formaFinantareData);
        this.setState({formeFinantare: listaFormeFinantare});
      });
    });
  }

  /**
   * folosita pentru a forta reactualizarea listei de studenti
   * @param index scroleaza pana la un index adiacent celui al studentului cazat
   */
  actualizeazaListaStudenti = (index) => {
    const indexToScrollTo =
      index + 2 < this.props.listaStudenti.length - 1
        ? index + 2
        : this.props.listaStudenti.length - 1; // ca sa nu scrolleze la ceva ce nu exista
    this.setState((prevState) => {
      return {
        keyVirtualizedList: prevState.keyVirtualizedList + 1, //se actualizeaza key-ul pentru a forta o rerandare
        virtualizedListIndexToScrollTo: indexToScrollTo, //se scroleaza la pozitia din lista unde era studentul
      };
    });
  };

  renderRow = ({index, parent, key, style}, lista) => {
    return (
      <CellMeasurer
        key={key}
        cache={cache}
        parent={parent}
        rowIndex={index}
        columnIndex={0}
      >
        <div style={style}>
          <Student
            id={lista[index].ID_Student}
            idFacultate={this.props.idFacultate}
            student={lista[index]}
            key={lista[index].ID_Student}
            nume={lista[index].NumeIntreg}
            culoareFacultate={this.props.culoareFacultate}
            afiseazaDoarCeiCuCerere={this.state.afiseazaDoarCeiCuCerere}
            idAnCazare={this.props.anCazare.ID_AnUniv}
            anCazare={this.props.anCazare}
            actualizeazaListaStudenti={() =>
              this.actualizeazaListaStudenti(index)
            }
            actualizeazaLabels={this.props.actualizeazaLabels.bind(this)}
            schimbaStatusStudent={this.props.schimbaStatusStudent.bind(this)}
            actualizeazaTipCameraMF={this.props.actualizeazaTipCameraMF.bind(
              this
            )}
            eStudentAdmitere={eStudentAdmitere(
              lista[index],
              this.props.anCazare.ID_AnUniv
            )}
            ID_TipCazare={this.props.ID_TipCazare}
            cazareDataDeCand={this.props.cazareDataDeCand}
            cazareDataPanaCand={this.props.cazareDataPanaCand}
            tipRandareStudent={"listaStudenti"}
          />
        </div>
      </CellMeasurer>
    );
  };

  handleInputChange = (e) => {
    this.setState({cautare: e.target.value});
  };

  selecteazaSpecializare = (e, specializare) => {
    this.setState({specializareSelectata: specializare.value});
  };

  selecteazaAnStudiu = (e, anStudiu) => {
    this.setState({anStudiuSelectat: anStudiu.value});
  };

  selecteazaFormaFinantare = (e, formaFinantare) => {
    this.setState({
      formaFinantareSelectata: formaFinantare.value,
    });
  };

  afiseazaCuCerere = () => {
    let bool = !this.state.afiseazaDoarCeiCuCerere;
    this.setState({afiseazaDoarCeiCuCerere: bool});
  };

  handleAfiseaza = (filterToApplly, filtersToRemove = []) => {
    let newState = {...this.state.afiseazaFields};
    let bool = !this.state.afiseazaFields[filterToApplly];

    newState[filterToApplly] = bool;
    filtersToRemove.forEach((filterToRemove) => {
      newState[filterToRemove] = false;
    });

    this.setState({afiseazaFields: newState});
  };

  handleOrdoneaza = (filterToApplly) => {
    let newState = {...this.state.ordoneazaFields};
    let bool = !this.state.ordoneazaFields[filterToApplly];

    for (const prop in newState) {
      newState[prop] = false;
    }
    newState[filterToApplly] = bool;

    this.setState({ordoneazaFields: newState});
  };

  handleGrupeaza = (filterToApplly) => {
    let newState = {...this.state.grupeazaFields};
    let bool = !this.state.grupeazaFields[filterToApplly];

    for (const prop in newState) {
      newState[prop] = false;
    }
    newState[filterToApplly] = bool;

    this.setState({grupeazaFields: newState});
  };

  grupare = (listaStudenti) => {
    //grupeaza apoi ordoneaza pe grupe
    let listaGrupata = [];
    let listaGrupata1D = [];

    if (this.state.grupeazaFields.grupeazaDupaLocalitate) {
      listaStudenti = listaStudenti.sort((a, b) => {
        if (
          a.DenumireJudet.normalize("NFD")
            .toLowerCase()
            .replace(/[\u0300-\u036f]/g, "")
            .replace(/\s/g, "") <
          b.DenumireJudet.normalize("NFD")
            .toLowerCase()
            .replace(/[\u0300-\u036f]/g, "")
            .replace(/\s/g, "")
        ) {
          return -1;
        } else if (
          a.DenumireJudet.normalize("NFD")
            .toLowerCase()
            .replace(/[\u0300-\u036f]/g, "")
            .replace(/\s/g, "") >
          b.DenumireJudet.normalize("NFD")
            .toLowerCase()
            .replace(/[\u0300-\u036f]/g, "")
            .replace(/\s/g, "")
        ) {
          return 1;
        } else {
          if (
            a.DenumireLocalitate.normalize("NFD")
              .toLowerCase()
              .replace(/[\u0300-\u036f]/g, "")
              .replace(/\s/g, "") <
            b.DenumireLocalitate.normalize("NFD")
              .toLowerCase()
              .replace(/[\u0300-\u036f]/g, "")
              .replace(/\s/g, "")
          ) {
            return -1;
          } else if (
            a.DenumireLocalitate.normalize("NFD")
              .toLowerCase()
              .replace(/[\u0300-\u036f]/g, "")
              .replace(/\s/g, "") >
            b.DenumireLocalitate.normalize("NFD")
              .toLowerCase()
              .replace(/[\u0300-\u036f]/g, "")
              .replace(/\s/g, "")
          ) {
            return 1;
          } else return 0;
        }
      });

      listaGrupata = groupBy(listaStudenti, "DenumireJudet");
      for (let judet in listaGrupata) {
        listaGrupata[judet] = groupBy(
          listaGrupata[judet],
          "DenumireLocalitate"
        );
      }
      listaGrupata.forEach((judet) => {
        judet.forEach((localitate) => {
          localitate.forEach((student) => {
            listaGrupata1D.push(student);
          });
        });
      });
    }

    if (this.state.grupeazaFields.grupeazaDupaUltimaCazare) {
      listaStudenti = listaStudenti.filter((el) => el.ID_UltimaCazare !== -1);
      listaGrupata = groupBy(listaStudenti, "DenumireCaminUltimaCazare");
      for (let grupaCamin in listaGrupata) {
        listaGrupata[grupaCamin] = groupBy(
          listaGrupata[grupaCamin],
          "NumarCameraUltimaCazare"
        );
      }
      listaGrupata.forEach((camin) => {
        camin.forEach((camera) => {
          camera.forEach((student) => {
            listaGrupata1D.push(student);
          });
        });
      });
    }

    if (this.state.grupeazaFields.grupeazaDupaGrupa) {
      listaGrupata = groupBy(listaStudenti, "DenumireGrupa");

      //revenire in 1D
      listaGrupata.forEach((grupa) => {
        grupa.forEach((student) => {
          listaGrupata1D.push(student);
        });
      });
    }
    return listaGrupata1D;
  };

  render() {
    //cauta student
    let temp = [];
    if (this.state.afiseazaDoarCeiCuCerere) {
      temp = this.props.listaStudenti.filter(
        (el) =>
          functieCautareTextStr(el.NumeIntreg, this.state.cautare) &&
          el.ID_CererePrecazare !== -1 &&
          el.CazareConfirmata === false
      );
    } else {
      temp = this.props.listaStudenti.filter((el) =>
        functieCautareTextStr(el.NumeIntreg, this.state.cautare)
      );
    }
    let listaFiltrata = temp;

    //afiseaza cei cu cerere
    if (this.state.afiseazaDoarCeiCuCerere) {
      listaFiltrata = listaFiltrata.filter(
        (el) => el.ID_CererePrecazare !== -1 && el.CazareConfirmata === false
      );
    }

    if (this.state.afiseazaFields.afiseazaStudentiAdmitere) {
      listaFiltrata = listaFiltrata.filter((el) =>
        eStudentAdmitere(el, this.props.anCazare.ID_AnUniv)
      );
    }

    if (this.state.afiseazaFields.afiseazaStudentiCazareVara) {
      listaFiltrata = listaFiltrata.filter((el) => el.ID_CazareVara != null);
    }

    if (this.state.afiseazaFields.afiseazaFete) {
      listaFiltrata = listaFiltrata.filter((el) => el.Sex === "F");
    }

    if (this.state.afiseazaFields.afiseazaBaieti) {
      listaFiltrata = listaFiltrata.filter((el) => el.Sex === "M");
    }

    if (this.state.afiseazaFields.afiseazaLicenta) {
      listaFiltrata = listaFiltrata.filter((el) => el.ID_TipCiclu === 2);
    }

    if (this.state.afiseazaFields.afiseazaMaster) {
      listaFiltrata = listaFiltrata.filter((el) => el.ID_TipCiclu === 4);
    }

    if (this.state.afiseazaFields.afiseazaOlimpici) {
      listaFiltrata = listaFiltrata.filter((el) => el.CandidatOlimpic === true);
    }

    if (this.state.afiseazaFields.afiseazaOrfani) {
      listaFiltrata = listaFiltrata.filter(
        (el) => el.Stare_sociala_speciala.indexOf("ambii") !== -1
      );
    }

    if (this.state.afiseazaFields.afiseazaCasaDeCopii) {
      listaFiltrata = listaFiltrata.filter(
        (el) => el.Stare_sociala_speciala !== "Nu e cazul"
      );
    }

    if (this.state.afiseazaFields.afiseazaProblemeMedicale) {
      listaFiltrata = listaFiltrata.filter(
        (el) => el.Situatie_medicala_speciala !== "Nu e cazul"
      );
    }

    if (this.state.afiseazaFields.afiseazaNrRestante2) {
      listaFiltrata = listaFiltrata.filter(
        (el) =>
          el.CazareNrExameneCreditate <= 2 &&
          eStudentAdmitere(el, this.props.anCazare.ID_AnUniv) === false
      );
    }

    if (this.state.specializareSelectata !== null)
      if (this.state.specializareSelectata !== -1) {
        let denumireSpecializare = "";
        this.state.specializari.forEach((specializare) => {
          if (specializare.value === this.state.specializareSelectata)
            denumireSpecializare = specializare.text;
        });
        denumireSpecializare = denumireSpecializare.substring(
          0,
          denumireSpecializare.indexOf("...")
        );
        //console.log(denumireSpecializare);
        listaFiltrata = listaFiltrata.filter(
          (el) =>
            el.DenumireSpecializare.normalize("NFD")
              .toLowerCase()
              .replace(/[\u0300-\u036f]/g, "")
              .replace(/\s/g, "") ===
            denumireSpecializare
              .normalize("NFD")
              .toLowerCase()
              .replace(/[\u0300-\u036f]/g, "")
              .replace(/\s/g, "")
        );
      }

    if (this.state.anStudiuSelectat !== null)
      if (this.state.anStudiuSelectat !== -1) {
        listaFiltrata = listaFiltrata.filter(
          (el) => el.ID_AnStudiu === this.state.anStudiuSelectat
        );
      }

    if (this.state.formaFinantareSelectata !== null)
      if (this.state.formaFinantareSelectata !== -1) {
        listaFiltrata = listaFiltrata.filter(
          (el) =>
            el.DenumireFormaFinantareInterna.normalize("NFD")
              .toLowerCase()
              .replace(/[\u0300-\u036f]/g, "")
              .replace(/\s/g, "") ===
            this.state.formeFinantare[
            this.state.formaFinantareSelectata + 1
              ].text
              .normalize("NFD")
              .toLowerCase()
              .replace(/[\u0300-\u036f]/g, "")
              .replace(/\s/g, "")
        );
      }
    //ordonare
    if (this.state.ordoneazaFields.ordoneazaCazareRestante) {
      listaFiltrata = listaFiltrata.filter(
        (el) =>
          el.CazareNrExameneCreditate > 0 &&
          eStudentAdmitere(el, this.props.anCazare.ID_AnUniv) === false
      );
      listaFiltrata = listaFiltrata.sort((a, b) =>
        a.CazareNrExameneCreditate > b.CazareNrExameneCreditate ? 1 : -1
      );
    }

    if (this.state.ordoneazaFields.ordoneazaCazareMedie) {
      let listaSortata = [];

      let listaIntegralisti = listaFiltrata.filter(
        (el) =>
          eStudentAdmitere(el, this.props.anCazare.ID_AnUniv) === false &&
          el.CazareNrExameneCreditate <= 0
      );
      listaIntegralisti = listaIntegralisti.sort((a, b) => {
        return a.CazareMedieAnCuFacultative < b.CazareMedieAnCuFacultative
          ? 1
          : -1;
      });
      listaSortata = listaSortata.concat(listaIntegralisti);

      let listaRestantieri = listaFiltrata.filter(
        (el) =>
          eStudentAdmitere(el, this.props.anCazare.ID_AnUniv) === false &&
          el.CazareNrExameneCreditate > 0
      );
      listaRestantieri = listaRestantieri.sort((a, b) =>
        a.CazareNrExameneCreditate > b.CazareNrExameneCreditate ? 1 : -1
      );
      listaSortata = listaSortata.concat(listaRestantieri);
      listaFiltrata = listaSortata;
    }

    if (this.state.ordoneazaFields.ordoneazaCazarePunctaj) {
      let listaSortata = [];

      let listaIntegralisti = listaFiltrata.filter(
        (el) =>
          eStudentAdmitere(el, this.props.anCazare.ID_AnUniv) === false &&
          el.CazareNrExameneCreditate <= 0
      );
      listaIntegralisti = listaIntegralisti.sort((a, b) => {
        return a.CazareMediaPunctajValoric < b.CazareMediaPunctajValoric
          ? 1
          : -1;
      });
      listaSortata = listaSortata.concat(listaIntegralisti);

      let listaRestantieri = listaFiltrata.filter(
        (el) =>
          eStudentAdmitere(el, this.props.anCazare.ID_AnUniv) === false &&
          el.CazareNrExameneCreditate > 0
      );
      listaRestantieri = listaRestantieri.sort((a, b) =>
        a.CazareNrExameneCreditate > b.CazareNrExameneCreditate ? 1 : -1
      );
      listaSortata = listaSortata.concat(listaRestantieri);
      listaFiltrata = listaSortata;
    }

    if (this.state.ordoneazaFields.ordoneazaRestante) {
      listaFiltrata = listaFiltrata.filter(
        (el) =>
          el.NrExameneCreditate > 0 &&
          eStudentAdmitere(el, this.props.anCazare.ID_AnUniv) === false
      );
      listaFiltrata = listaFiltrata.sort((a, b) =>
        a.NrExameneCreditate > b.NrExameneCreditate ? 1 : -1
      );
    }

    if (this.state.ordoneazaFields.ordoneazaMedie) {
      let listaSortata = [];

      let listaIntegralisti = listaFiltrata.filter(
        (el) =>
          eStudentAdmitere(el, this.props.anCazare.ID_AnUniv) === false &&
          el.NrExameneCreditate <= 0
      );
      listaIntegralisti = listaIntegralisti.sort((a, b) => {
        return a.MediaAnCuFacultative < b.MediaAnCuFacultative ? 1 : -1;
      });
      listaSortata = listaSortata.concat(listaIntegralisti);

      let listaRestantieri = listaFiltrata.filter(
        (el) =>
          eStudentAdmitere(el, this.props.anCazare.ID_AnUniv) === false &&
          el.NrExameneCreditate > 0
      );
      listaRestantieri = listaRestantieri.sort((a, b) =>
        a.NrExameneCreditate > b.NrExameneCreditate ? 1 : -1
      );
      listaSortata = listaSortata.concat(listaRestantieri);
      listaFiltrata = listaSortata;
    }

    if (this.state.ordoneazaFields.ordoneazaPunctaj) {
      let listaSortata = [];

      let listaIntegralisti = listaFiltrata.filter(
        (el) =>
          eStudentAdmitere(el, this.props.anCazare.ID_AnUniv) === false &&
          el.NrExameneCreditate <= 0
      );
      listaIntegralisti = listaIntegralisti.sort((a, b) => {
        return a.PunctajDacaIntegralist_An < b.PunctajDacaIntegralist_An
          ? 1
          : -1;
      });
      listaSortata = listaSortata.concat(listaIntegralisti);

      let listaRestantieri = listaFiltrata.filter(
        (el) =>
          eStudentAdmitere(el, this.props.anCazare.ID_AnUniv) === false &&
          el.NrExameneCreditate > 0
      );
      listaRestantieri = listaRestantieri.sort((a, b) =>
        a.NrExameneCreditate > b.NrExameneCreditate ? 1 : -1
      );
      listaSortata = listaSortata.concat(listaRestantieri);
      listaFiltrata = listaSortata;
    }

    if (this.state.ordoneazaFields.ordoneazaMedieAdmitere) {
      listaFiltrata = listaFiltrata.filter((el) =>
        eStudentAdmitere(el, this.props.anCazare.ID_AnUniv)
      );
      listaFiltrata = listaFiltrata.sort((a, b) => {
        return a.MedieAdmitere < b.MedieAdmitere ? 1 : -1;
      });
    }
    if (this.state.ordoneazaFields.ordoneazaAlfabeticAZ) {
      listaFiltrata = listaFiltrata.sort((a, b) =>
        a.NumeIntreg > b.NumeIntreg ? 1 : -1
      );
    }
    if (this.state.ordoneazaFields.ordoneazaAlfabeticZA) {
      listaFiltrata = listaFiltrata.sort((a, b) =>
        a.NumeIntreg < b.NumeIntreg ? 1 : -1
      );
    }
    if (this.state.ordoneazaFields.ordineAnStudiuAscendent) {
      listaFiltrata = listaFiltrata.sort((a, b) => {
        if (a.DenumireCicluInv === b.DenumireCicluInv) {
          if (
            a.DenumireAnStudiu.toLowerCase() ===
            b.DenumireAnStudiu.toLowerCase()
          )
            if (a.ID_AnUniv < b.ID_AnUniv) return 1;
            else return -1;
          if (
            a.DenumireAnStudiu.toLowerCase() < b.DenumireAnStudiu.toLowerCase()
          )
            return -1;
          if (
            a.DenumireAnStudiu.toLowerCase() > b.DenumireAnStudiu.toLowerCase()
          )
            return 1;
        } else {
          if (
            a.DenumireCicluInv.toLowerCase().replace(/\s/g, "") === "doctorat"
          )
            return 1;
          else if (
            a.DenumireCicluInv.toLowerCase().replace(/\s/g, "") === "master" &&
            b.DenumireCicluInv.toLowerCase().replace(/\s/g, "") === "licenta"
          )
            return 1;
          else return -1;
        }
        return 0;
      });
    }
    if (this.state.ordoneazaFields.ordineAnStudiuDescendent) {
      listaFiltrata = listaFiltrata.sort((a, b) => {
        if (a.DenumireCicluInv === b.DenumireCicluInv) {
          if (
            a.DenumireAnStudiu.toLowerCase() ===
            b.DenumireAnStudiu.toLowerCase()
          )
            if (a.ID_AnUniv < b.ID_AnUniv) return 1;
            else return -1;
          if (
            a.DenumireAnStudiu.toLowerCase() > b.DenumireAnStudiu.toLowerCase()
          )
            return -1;
          if (
            a.DenumireAnStudiu.toLowerCase() < b.DenumireAnStudiu.toLowerCase()
          )
            return 1;
        } else {
          if (
            a.DenumireCicluInv.toLowerCase().replace(/\s/g, "") === "doctorat"
          )
            return 1;
          else if (
            a.DenumireCicluInv.toLowerCase().replace(/\s/g, "") === "master" &&
            b.DenumireCicluInv.toLowerCase().replace(/\s/g, "") === "licenta"
          )
            return -1;
          else return 1;
        }
        return 0;
      });
    }
    if (
      this.state.grupeazaFields.grupeazaDupaGrupa ||
      this.state.grupeazaFields.grupeazaDupaLocalitate ||
      this.state.grupeazaFields.grupeazaDupaUltimaCazare
    )
      listaFiltrata = this.grupare(listaFiltrata);
    //console.log(listaFiltrata);

    return (
      /*componentul randeaza un grid cu 3 randuri
                   primul e meniul de filtrare si ordonare
                   al doilea e lista de studenti, randata eficient
                   cu react-virtualized*/
      <Grid rows={3} columns="equal">
        <Grid.Row
          className="banner"
          verticalAlign="middle"
          style={{
            backgroundColor: this.props.culoareFacultate,
          }}
          columns={5}
        >
          <Grid.Column className="bannerColumn">
            <Button
              type="button"
              toggle
              active={this.state.afiseazaDoarCeiCuCerere}
              className="butonFiltru"
              onClick={this.afiseazaCuCerere}
              icon="file text outline"
              content="Afișează doar cu cerere"
            />
          </Grid.Column>
          <Grid.Column className="bannerColumn">
            <Popup
              trigger={
                <Button
                  type="button"
                  className="butonFiltru"
                  icon="filter"
                  content="AFIȘEAZĂ"
                />
              }
              hoverable
              position="bottom left"
              flowing
              basic
            >
              <Form>
                <Form.Group grouped>
                  <Form.Checkbox
                    label={
                      <label className="checkboxStyle">
                        {" "}
                        Studenți anul I (Admitere)
                      </label>
                    }
                    onClick={() =>
                      this.handleAfiseaza("afiseazaStudentiAdmitere")
                    }
                    checked={this.state.afiseazaFields.afiseazaStudentiAdmitere}
                  />
                  <Form.Checkbox
                    label={
                      <label className="checkboxStyle">
                        {" "}
                        Studenți care au avut cazare pe vara
                      </label>
                    }
                    onClick={() =>
                      this.handleAfiseaza("afiseazaStudentiCazareVara")
                    }
                    checked={this.state.afiseazaFields.afiseazaStudentiCazareVara}
                  />
                  <Form.Checkbox
                    label={<label className="checkboxStyle"> Fete</label>}
                    onClick={() =>
                      this.handleAfiseaza("afiseazaFete", ["afiseazaBaieti"])
                    }
                    checked={this.state.afiseazaFields.afiseazaFete}
                  />
                  <Form.Checkbox
                    label={<label className="checkboxStyle"> Băieți</label>}
                    onClick={() =>
                      this.handleAfiseaza("afiseazaBaieti", ["afiseazaFete"])
                    }
                    checked={this.state.afiseazaFields.afiseazaBaieti}
                  />
                  <Form.Checkbox
                    label={
                      <label className="checkboxStyle"> Studenți licență</label>
                    }
                    onClick={() =>
                      this.handleAfiseaza("afiseazaLicenta", ["afiseazaMaster"])
                    }
                    checked={this.state.afiseazaFields.afiseazaLicenta}
                  />
                  <Form.Checkbox
                    label={
                      <label className="checkboxStyle"> Studenți master</label>
                    }
                    onClick={() =>
                      this.handleAfiseaza("afiseazaMaster", ["afiseazaLicenta"])
                    }
                    checked={this.state.afiseazaFields.afiseazaMaster}
                  />
                  <Form.Checkbox
                    label={<label className="checkboxStyle"> Olimpici</label>}
                    onClick={() => this.handleAfiseaza("afiseazaOlimpici")}
                    checked={this.state.afiseazaFields.afiseazaOlimpici}
                  />
                  <Form.Checkbox
                    label={
                      <label className="checkboxStyle">
                        {" "}
                        Orfani (ambii părinți)
                      </label>
                    }
                    onClick={() => this.handleAfiseaza("afiseazaOrfani")}
                    checked={this.state.afiseazaFields.afiseazaOrfani}
                  />
                  <Form.Checkbox
                    label={
                      <label className="checkboxStyle">
                        {" "}
                        Studenți cu situație socială specială
                      </label>
                    }
                    onClick={() => this.handleAfiseaza("afiseazaCasaDeCopii")}
                    checked={this.state.afiseazaFields.afiseazaCasaDeCopii}
                  />
                  <Form.Checkbox
                    label={
                      <label className="checkboxStyle">
                        {" "}
                        Studenți cu probleme medicale
                      </label>
                    }
                    onClick={() =>
                      this.handleAfiseaza("afiseazaProblemeMedicale")
                    }
                    checked={this.state.afiseazaFields.afiseazaProblemeMedicale}
                  />
                  <Form.Checkbox
                    label={
                      <label className="checkboxStyle">
                        {"Studenți cu număr restanțe <= 2"}
                      </label>
                    }
                    onClick={() => this.handleAfiseaza("afiseazaNrRestante2")}
                    checked={this.state.afiseazaFields.afiseazaNrRestante2}
                  />
                </Form.Group>
              </Form>
              <Dropdown
                selection
                value={this.state.specializareSelectata}
                onChange={this.selecteazaSpecializare}
                options={this.state.specializari}
                placeholder="Specializare"
                className="dropdownStyle"
              />
              <br/>
              <Dropdown
                selection
                value={this.state.anStudiuSelectat}
                onChange={this.selecteazaAnStudiu}
                options={this.state.aniStudiu}
                placeholder="An studiu"
                className="dropdownStyle"
              />
              <br/>
              <Dropdown
                selection
                value={this.state.formaFinantareSelectata}
                onChange={this.selecteazaFormaFinantare}
                options={this.state.formeFinantare}
                placeholder="Formă finanțare"
                className="dropdownStyle"
              />
            </Popup>
          </Grid.Column>
          <Grid.Column className="bannerColumn">
            <Popup
              trigger={
                <Button
                  type="button"
                  className="butonFiltru"
                  icon="sort amount up"
                  content="ORDONEAZĂ"
                />
              }
              hoverable
              position="bottom left"
              flowing
              basic
            >
              <Form>
                <Form.Group grouped>
                  <Form.Checkbox
                    label={
                      <label className="checkboxStyle">
                        {" "}
                        {"După număr restanțe "}
                        <Icon className="sort numeric down"/>
                      </label>
                    }
                    onClick={() => this.handleOrdoneaza("ordoneazaRestante")}
                    checked={this.state.ordoneazaFields.ordoneazaRestante}
                  />
                  <Form.Checkbox
                    label={
                      <label className="checkboxStyle">
                        {" "}
                        {"După medie "}
                        <Icon className="sort numeric descending"/>
                      </label>
                    }
                    onClick={() => this.handleOrdoneaza("ordoneazaMedie")}
                    checked={this.state.ordoneazaFields.ordoneazaMedie}
                  />
                  <Form.Checkbox
                    label={
                      <label className="checkboxStyle">
                        {" "}
                        {"După punctaj "}
                        <Icon className="sort numeric descending"/>
                      </label>
                    }
                    onClick={() => this.handleOrdoneaza("ordoneazaPunctaj")}
                    checked={this.state.ordoneazaFields.ordoneazaPunctaj}
                  />
                  <Form.Checkbox
                    label={
                      <label className="checkboxStyle">
                        {" "}
                        {"După număr restanțe (15 Sept) "}
                        <Icon className="sort numeric down"/>
                      </label>
                    }
                    onClick={() =>
                      this.handleOrdoneaza("ordoneazaCazareRestante")
                    }
                    checked={this.state.ordoneazaFields.ordoneazaCazareRestante}
                  />
                  <Form.Checkbox
                    label={
                      <label className="checkboxStyle">
                        {" "}
                        {"După medie (15 Sept)"}
                        <Icon className="sort numeric descending"/>
                      </label>
                    }
                    onClick={() => this.handleOrdoneaza("ordoneazaCazareMedie")}
                    checked={this.state.ordoneazaFields.ordoneazaCazareMedie}
                  />
                  <Form.Checkbox
                    label={
                      <label className="checkboxStyle">
                        {" "}
                        {"După punctaj (15 Sept)"}
                        <Icon className="sort numeric descending"/>
                      </label>
                    }
                    onClick={() =>
                      this.handleOrdoneaza("ordoneazaCazarePunctaj")
                    }
                    checked={this.state.ordoneazaFields.ordoneazaCazarePunctaj}
                  />
                  <Form.Checkbox
                    label={
                      <label className="checkboxStyle">
                        {" "}
                        {"După media de admitere "}
                        <Icon className="sort numeric descending"/>
                      </label>
                    }
                    onClick={() =>
                      this.handleOrdoneaza("ordoneazaMedieAdmitere")
                    }
                    checked={this.state.ordoneazaFields.ordoneazaMedieAdmitere}
                  />
                  <Form.Checkbox
                    label={
                      <label className="checkboxStyle">
                        {" "}
                        {"După an "}
                        <Icon className="sort content descending"/>
                      </label>
                    }
                    onClick={() =>
                      this.handleOrdoneaza("ordineAnStudiuAscendent")
                    }
                    checked={this.state.ordoneazaFields.ordineAnStudiuAscendent}
                  />
                  <Form.Checkbox
                    label={
                      <label className="checkboxStyle">
                        {" "}
                        {"După an "}
                        <Icon className="sort content ascending"/>
                      </label>
                    }
                    onClick={() =>
                      this.handleOrdoneaza("ordineAnStudiuDescendent")
                    }
                    checked={
                      this.state.ordoneazaFields.ordineAnStudiuDescendent
                    }
                  />
                  <Form.Checkbox
                    label={
                      <label className="checkboxStyle">
                        {" "}
                        {"Alfabetic "}
                        <Icon className="sort alphabet down"/>
                      </label>
                    }
                    onClick={() => this.handleOrdoneaza("ordoneazaAlfabeticAZ")}
                    checked={this.state.ordoneazaFields.ordoneazaAlfabeticAZ}
                  />
                  <Form.Checkbox
                    label={
                      <label className="checkboxStyle">
                        {" "}
                        {"Alfabetic "}
                        <Icon className="sort alphabet up"/>
                      </label>
                    }
                    onClick={() => this.handleOrdoneaza("ordoneazaAlfabeticZA")}
                    checked={this.state.ordoneazaFields.ordoneazaAlfabeticZA}
                  />
                </Form.Group>
              </Form>
            </Popup>
          </Grid.Column>
          <Grid.Column className="bannerColumn">
            <Popup
              trigger={
                <Button
                  type="button"
                  className="butonFiltru"
                  icon="group"
                  content="GRUPEAZĂ"
                />
              }
              hoverable
              position="bottom left"
              flowing
              basic
            >
              <Form>
                <Form.Group grouped>
                  <Form.Checkbox
                    label={
                      <label className="checkboxStyle">
                        {" "}
                        {"După localitate"}
                      </label>
                    }
                    onClick={() =>
                      this.handleGrupeaza("grupeazaDupaLocalitate")
                    }
                    checked={this.state.grupeazaFields.grupeazaDupaLocalitate}
                  />
                  <Form.Checkbox
                    label={
                      <label className="checkboxStyle">
                        {" "}
                        {"După ultima cazare"}
                      </label>
                    }
                    onClick={() =>
                      this.handleGrupeaza("grupeazaDupaUltimaCazare")
                    }
                    checked={this.state.grupeazaFields.grupeazaDupaUltimaCazare}
                  />
                  <Form.Checkbox
                    label={
                      <label className="checkboxStyle"> {"După grupă"}</label>
                    }
                    onClick={() => this.handleGrupeaza("grupeazaDupaGrupa")}
                    checked={this.state.grupeazaFields.grupeazaDupaGrupa}
                  />
                </Form.Group>
              </Form>
            </Popup>
          </Grid.Column>
          <Grid.Column className="bannerColumn">
            <Form>
              <TextArea
                className="cautare"
                placeholder="Caută student ..."
                value={this.state.cautare}
                onChange={this.handleInputChange}
              />
            </Form>
          </Grid.Column>
        </Grid.Row>

        <Grid.Row columns="equal">
          <Grid.Column className="labelSeparatorCazare"/>
          <Grid.Column className="labelCazare">
            <Popup
              trigger={
                <div>
                  <LabelStatistici
                    icon="minus"
                    title="Cu cerere de precazare"
                    culoareFacultate={"green"}
                  />
                </div>
              }
              content="Dacă se alege afișarea tuturor studenților, legenda îi evidențiază pe cei care au depus cerere de precazare."
              position="bottom center"
            />
          </Grid.Column>
          <Grid.Column className="labelSeparatorCazare"/>
          <Grid.Column className="labelCazare">
            <LabelStatistici
              icon="address card"
              title="Rezultatele filtrării:"
              culoareFacultate={this.props.culoareFacultate}
              detail={listaFiltrata.length}
            />
          </Grid.Column>
          <Grid.Column className="labelSeparatorCazare"/>
          <Grid.Column className="labelCazare">
            <Popup
              trigger={
                <div>
                  <LabelStatistici
                    icon="minus"
                    title="Cu cazare neconfirmată decazat"
                    culoareFacultate={"orange"}
                  />
                </div>
              }
              content="Dacă studentul se află în cameră, legenda semnifică faptul că studentul nu are cazarea confirmată."
              position="bottom center"
            />
          </Grid.Column>
          <Grid.Column className="labelSeparatorCazare"/>
        </Grid.Row>

        <Grid.Row>
          <Grid.Column textAlign="center">
            {listaFiltrata.length === 0 ? (
              this.props.listaStudenti.filter(
                (el) => el.ID_CererePrecazare !== -1
              ).length === 0 && this.state.afiseazaDoarCeiCuCerere ? (
                <div>
                  <Message compact info>
                    Nu există cereri de precazare.
                  </Message>
                </div>
              ) : (
                <div>
                  <Message compact info>
                    Nu au fost găsite rezultate.
                  </Message>
                </div>
              )
            ) : (
              <AutoSizer style={droppableStyle}>
                {({width, height}) => {
                  return (
                    <List
                      key={this.state.keyVirtualizedList}
                      height={height}
                      width={width}
                      rowHeight={cache.rowHeight}
                      /*lungimea listei de studenti*/
                      rowCount={listaFiltrata.length}
                      /*cache se foloseste la randarea temporara a datelor vizibile*/
                      deferredMeasurementCache={cache}
                      /*functie de randare a fiecarui rand/element din lista*/
                      rowRenderer={({index, parent, key, style}) =>
                        this.renderRow(
                          {index, parent, key, style},
                          listaFiltrata
                        )
                      }
                      overscanRowCount={5} //cati studenti sa se incarce in plus fata de zona vizibila
                      scrollToIndex={this.state.virtualizedListIndexToScrollTo}
                    />
                  );
                }}
              </AutoSizer>
            )}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

export default ListaStudenti;
