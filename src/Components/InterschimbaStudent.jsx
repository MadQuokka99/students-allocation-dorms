import {
    Button,
    Container,
    Dropdown,
    Header,
    Icon,
    Input, Label,
    List,
    Loader,
    Message,
    Modal,
    Popup,
    Segment
} from "semantic-ui-react";
import React, {Component} from "react";
import axiosRef from "../axios-AGSIS-API-refactor";

/**
 * componenta afiseaza un Modal in care utilizatorul poate cauta un student pentru a-l interschimba cu studentul selectat
 */
class InterschimbaStudent extends Component {


    constructor(props) {
        super(props);
        this.state = {
            modalInterschimbareOpen: false,

            numeStudentCautat: "",
            buttonCautaStudentServerSide: false,
            loaderListaStudenti: false,

            listaStudentiCautatiServerSide: [],

            studentSelectat: null

        }
    }

    handleOpenModalInterschimbare = () => {
        this.setState({
            modalInterschimbareOpen: true
        })
    }

    handleCloseModalInterschimbare = () => {
        this.setState({
            modalInterschimbareOpen: false
        })
    }

    handleOpenModalAtentionareInterschimbare = (student) => {
        this.setState({
            modalAtentionareInterschimbareOpen: true,
            studentSelectat: student
        })
    }

    handleCloseModalAtentionareInterschimbare = () => {
        this.setState({
            modalAtentionareInterschimbareOpen: false
        })
    }

    /**
     * Modalul apare atunci cand un student este selectat din search-ul de cautare si permite interschimbarea celor doi studenti
     */
    modalAtentionareInterschimbare = () => {
        return (
            <Modal closeIcon
                   size="small"
                   open={this.state.modalAtentionareInterschimbareOpen}
                   onClose={this.handleCloseModalAtentionareInterschimbare}
            >
                <Header icon="arrows alternate horizontal" content="Interschimbare studenți"/>
                <Modal.Content>
                    <p>Doriți interchimbarea studentului {this.props.student.NumeIntreg} cu studentul {this.state.studentSelectat}?</p>
                </Modal.Content>
                <Modal.Actions>
                    <Button type="button" basic color='green'>
                        Da
                    </Button>
                    <Button type="button" basic color='red' onClick={() => this.handleCloseModalAtentionareInterschimbare()}>
                        Nu
                    </Button>
                </Modal.Actions>
            </Modal>

        )
    }

    /**
     *
     * @param nume
     * @param ID_AnUniv
     * @param ID_Facultate
     * @param numarInregistrariMaximDeReturnat
     *
     * functia de cautare studenti pentru search-ul din modalul de interschimbare studenti
     */
    cautaStudentServerSide = (
        nume,
        ID_AnUniv,
        ID_Facultate,
        numarInregistrariMaximDeReturnat = 10
    ) => {
        this.setState({
            loaderListaStudenti: true,
        });
        let idFacultate;
        if (this.props.eSefAdminCamine) {
            idFacultate = -1;
        } else {
            idFacultate = ID_Facultate;
        }

        axiosRef
            .get(
                `Cazare/CazareCautaDupaNumeStudent?nume=${nume}&ID_AnUniv=${ID_AnUniv}&numarInregistrariMaximDeReturnat=${numarInregistrariMaximDeReturnat}&ID_Facultate=${idFacultate}`
            )
            .then((r) => {
                //console.log(r);
                this.setState({
                    listaStudentiCautatiServerSide: r.data,
                    loaderListaStudenti: false,
                });
            })
            .catch((e) => {
                alert("A aparut o eroare pe server :)");
                console.log(e.response);
                console.log(e.response.data);
            });
        this.setState({
            buttonCautaStudentServerSide: true,
        });
    };

    render() {
        return (
            <Modal
                closeIcon
                size="small"
                trigger={
                    <Dropdown.Item
                        onClick={this.handleOpenModalInterschimbare}
                        type="button"
                        text="Interschimbă studenții"
                    />
                }
                open={this.state.modalInterschimbareOpen}
                onClose={this.handleCloseModalInterschimbare}
            >
                <Header content="Căutați studentul cu care doriți să faceți interschimbarea" icon="arrows alternate horizontal"/>
                <Modal.Content>
                    <div className="inputSearchStudentiCazati">
                        <Input
                            className="cautare"
                            placeholder="Introdu numele de familie al studentului"
                            action
                            type="text"
                        >
                            <input
                                autoFocus={true}
                                onKeyPress={(e) => {
                                    //daca se apasa enter se initiaza cautarea
                                    //console.log(e.key);
                                    if (e.key === "Enter") {
                                        this.cautaStudentServerSide(
                                            this.state.numeStudentCautat,
                                            this.props.idAnCazare,
                                            this.props.idFacultate
                                        );
                                    }
                                }}
                                onChange={(e, value) => {
                                    this.setState({
                                        numeStudentCautat: e.target.value,
                                        buttonCautaStudentServerSide: false,
                                    });
                                }}
                            />
                            <Button
                                type="button"
                                onClick={() => {
                                    this.cautaStudentServerSide(
                                        this.state.numeStudentCautat,
                                        this.props.idAnCazare,
                                        this.props.idFacultate
                                    );
                                }}
                            >
                                Caută student
                            </Button>
                        </Input>
                    </div>
                    <List celled>
                        {this.state.loaderListaStudenti ? (
                            <Container textAlign="center">
                                <Loader active inline size="small"/>
                            </Container>
                        ) : this.state.listaStudentiCautatiServerSide.length === 0 &&
                        this.state.numeStudentCautat !== "" ? (
                            <div className="inputSearchStudentiCazati">
                                <Message
                                    hidden={!this.state.buttonCautaStudentServerSide}
                                    info
                                    compact
                                >
                                    Nu a fost găsit niciun student!
                                </Message>
                            </div>
                        ) : (
                            this.state.listaStudentiCautatiServerSide.map((student) => (
                                <Popup
                                    key={student.ID_Student}
                                    disabled={student.CazareExpirataLaDataCurenta}
                                    content="Apasă pe student pentru a-l selecta!"
                                    trigger={
                                        <List.Item
                                            onClick={() => {
                                                    this.handleOpenModalAtentionareInterschimbare(student);
                                            }}
                                        >
                                            <Icon
                                                name={
                                                    student.CazareExpirataLaDataCurenta === true
                                                        ? "male"
                                                        : student.CazareConfirmata
                                                        ? "check"
                                                        : !student.CazareConfirmata
                                                            ? "x"
                                                            : "x"
                                                }
                                                color={
                                                    student.CazareExpirataLaDataCurenta === true
                                                        ? "grey"
                                                        : student.CazareConfirmata
                                                        ? "green"
                                                        : !student.CazareConfirmata
                                                            ? "red"
                                                            : "black"
                                                }
                                            />
                                            <List.Content>
                                                {
                                                    this.props.eSefAdminCamine ?
                                                        (student.DenumireFacultate === null) ?
                                                            <List.Header>
                                                                {student.NumeIntreg}
                                                                <Popup trigger={
                                                                    <Icon name="exclamation circle" color="red"/>}
                                                                       content="Studentul nu are grupă și nu poate fi cazat"
                                                                />
                                                            </List.Header> :
                                                            <List.Header>{student.NumeIntreg + ' - ' + student.DenumireFacultate} </List.Header>
                                                        : <List.Header>{student.NumeIntreg} </List.Header>
                                                }
                                                {student.CazareExpirataLaDataCurenta === false
                                                    ? `${student.DenumireCamin} - Camera ${student.NumarCamera}`
                                                    : "Nu este cazat"}
                                            </List.Content>
                                        </List.Item>
                                    }
                                />
                            ))
                        )}
                    </List>

                </Modal.Content>

                <Modal.Actions>
                    <Segment floated="left" basic>
                        <Label>
                            <Icon name="check" color="green"/> Cazare confirmată
                        </Label>

                        <Label>
                            <Icon name="x" color="red"/> Cazare neconfirmată
                        </Label>
                        <Label>
                            <Icon name="male" color="grey"/> Nu este cazat
                        </Label>
                    </Segment>
                    <Button
                        style={{
                            marginTop: "auto",
                            marginBottom: "auto",
                        }}
                        type="button"
                        basic
                        color="red"
                        onClick={this.handleCloseModalInterschimbare}
                    >
                        Închide
                    </Button>
                </Modal.Actions>

                {
                    this.modalAtentionareInterschimbare()
                }
            </Modal>
        )
    }
}
export default InterschimbaStudent;
