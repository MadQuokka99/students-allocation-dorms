import React, { Component } from "react";
import { Button, Modal, List, Icon, Dropdown } from "semantic-ui-react";
import { SingleDatePicker } from "react-dates";
import moment from "moment";

import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";

//sets the language that will be used by react-dates
import "moment/locale/ro";
import PropTypes from "prop-types";

import { descarcaRaportGeneric } from "../DescarcaRaport";

/**
 * Modal care permite descarcarea rapoartelor referitoare la un camin
 * @component
 */
class MeniuRapoarte extends Component {
  state = {
    modalOpen: false,
    calendarCNFIS_Focused: false,
    RaportCamereLibereCamin: {
      loading: false,
    },
    RAP_CaministiRestantieriLaPlata: {
      loading: false,
      semnePosibile: [
        { key: "<", value: "<", text: "<" },
        { key: "<=", value: "<=", text: "<=" },
        { key: ">", value: ">", text: ">" },
        { key: ">=", value: ">=", text: ">=" },
      ],
      semnSelectat: "",
      perioadaGratie: "",
    },
    raportCNFIS: {
      loading: false,
      data: moment().startOf("year"), //ia prima zi a anului curent
      tip: null,
    },
    RAP_StudentiCazarePentruAvizier: {
      loading: false,
    },
    RAP_SituatieOcupareCamine: {
      loading: false,
    },
    RAP_StudentiCazareCuMedie: {
      loading: false,
    },
    RAP_StudentiNeconfirmatiByFacultate:{
      loading:false
    },
    RAP_CamereLibereByFacultate:{
      loading: false
    },
    RAP_CereriRamanereCamin: {
      loading: false
    },
    RAP_ListaDeAsteptareCazare: {
      loading: false
    }
  };

  /**
   * Descarca raportul cu camerele libere pentru caminul si anul universitar selectat
   * @method
   */
  descarcaRaportCamereLibereCamin = () => {
    let param = [
      {
        name: "ID_Camin",
        value: this.props.ID_Camin.toString(),
      },
      {
        name: "ID_Facultate",
        value: this.props.ID_Facultate.toString()
      },
      {
        name: "ID_AnUniv",
        value: this.props.ID_AnUniv.toString(),
      },
      {
        name: "TipCazareAE",
        value: "A",
      },
    ];

    let rap = { ...this.state.RaportCamereLibereCamin };
    rap.loading = true;
    this.setState({ RaportCamereLibereCamin: rap });

    descarcaRaportGeneric("RSOcupareCameraByCaminGen", param)
      .catch(() => {
        alert("A aparut o eroare la descarcarea raportului");
      })
      .finally(() => {
        let rap = { ...this.state.RaportCamereLibereCamin };
        rap.loading = false;
        this.setState({ RaportCamereLibereCamin: rap });
      });
  };

  descarcaRap;

  /**
   * Descarca raportul cu camerele libere pentru caminul si anul universitar selectat
   * @method
   */
  descarcaRaportStudentiCazareCuMedie = () => {
    let param = [
      {
        Name: "ID_Facultate",
        Value: this.props.ID_Facultate.toString(),
      },

      {
        Name: "ID_AnUniv",
        Value: this.props.ID_AnUnivCazare.toString(),
      },
      {
        Name: "ID_Camin",
        Value: "-1",
      },
      {
        Name: "ID_TipCazare",
        Value: 1
      }
    ];

    console.log(param);
    let rap = { ...this.state.RAP_StudentiCazareCuMedie };
    rap.loading = true;
    this.setState({ RAP_StudentiCazareCuMedie: rap });

    descarcaRaportGeneric("RAP_StudentiCazarePentruAvizier", param)
      .catch(() => {
        alert("A aparut o eroare la descarcarea raportului");
      })
      .finally(() => {
        let rap = { ...this.state.RAP_StudentiCazareCuMedie };
        rap.loading = false;
        this.setState({ RAP_StudentiCazareCuMedie: rap });
      });
  };

  /**
   * Descarca raportul cu camerele libere pentru caminul si anul universitar selectat
   * @method
   */
  descarcaRaportCazareAvizier = () => {
    let param = [
      {
        Name: "ID_Facultate",
        Value: this.props.ID_Facultate.toString(),
      },

      {
        Name: "ID_AnUniv",
        Value: this.props.ID_AnUnivCazare.toString(),
      },
      {
        Name: "ID_Camin",
        Value: "-1",
      },
    ];

    console.log(param);
    let rap = { ...this.state.RAP_StudentiCazarePentruAvizier };
    rap.loading = true;
    this.setState({ RAP_StudentiCazarePentruAvizier: rap });

    descarcaRaportGeneric("RAP_StudentiCazarePentruAvizier", param)
      .catch(() => {
        alert("A aparut o eroare la descarcarea raportului");
      })
      .finally(() => {
        let rap = { ...this.state.RAP_StudentiCazarePentruAvizier };
        rap.loading = false;
        this.setState({ RAP_StudentiCazarePentruAvizier: rap });
      });
  };

  /**
   * Descarca raportul cu restantierii
   * @method
   */
  descarcaRaportRestantieriCamin = async () => {
    let param = [
      {
        name: "id_anUniv",
        value: this.props.ID_AnUniv.toString(),
      },
      {
        name: "perioadaGratie",
        value: this.state.RAP_CaministiRestantieriLaPlata.perioadaGratie,
      },
      {
        name: "idCamin",
        value: this.props.ID_Camin,
      },
      {
        name: "semn",
        value: this.state.RAP_CaministiRestantieriLaPlata.semnSelectat,
      },
    ];

    let dataRap = { ...this.state.RAP_CaministiRestantieriLaPlata };
    dataRap.loading = true;
    this.setState({ raportTaxe: dataRap });

    descarcaRaportGeneric("RAP_CaministiRestantieriLaPlata", param)
      .catch(() => {
        alert("A aparut o eroare la descarcarea raportului");
      })
      .finally(() => {
        let dataRap = { ...this.state.RAP_CaministiRestantieriLaPlata };
        dataRap.loading = false;
        this.setState({ raportTaxe: dataRap });
      });
  };

  /**
   * Descarca raportul cu situatia ocuparii caminului
   * @method
   */
  descarcaRaportSituatieOcupareCamine = () => {
    let param = [
      {
        Name: "ID_AnUniv",
        Value: this.props.ID_AnUnivCazare.toString(),
      },
    ];

    let rap = { ...this.state.RAP_SituatieOcupareCamine };
    rap.loading = true;
    this.setState({ RAP_SituatieOcupareCamine: rap });

    descarcaRaportGeneric("RAP_SituatieOcupareCamine", param)
      .catch(() => {
        alert("A aparut o eroare la descarcarea raportului");
      })
      .finally(() => {
        let rap = { ...this.state.RAP_SituatieOcupareCamine };
        rap.loading = false;
        this.setState({ RAP_SituatieOcupareCamine: rap });
      });
  };

  descarcaRaportStudentiNeconfirmatiByFacultate = () => {
    let param = [
      {
        Name: "ID_AnUniv",
        Value: this.props.ID_AnUnivCazare.toString(),
      },
      {
        Name: "ID_Facultate",
        Value: this.props.ID_Facultate.toString(),
      },
      {
        Name: "ID_Camin",
        Value: "-1"
      },
      {
        Name: "ID_TipCazare",
        Value: "1"
      }
    ];

    let rap = { ...this.state.RAP_StudentiNeconfirmatiByFacultate };
    rap.loading = true;
    this.setState({ RAP_StudentiNeconfirmatiByFacultate: rap });

    descarcaRaportGeneric("RAP_StudentiNeconfirmatiByFacultate", param)
      .catch(() => {
        alert("A aparut o eroare la descarcarea raportului");
      })
      .finally(() => {
        let rap = { ...this.state.RAP_StudentiNeconfirmatiByFacultate };
        rap.loading = false;
        this.setState({ RAP_StudentiNeconfirmatiByFacultate: rap });
      });
  };


  descarcaRaportCamereLibereByFacultate = () => {
    let param = [
      {
        Name: "ID_AnUniv",
        Value: this.props.ID_AnUnivCazare.toString(),
      },
      {
        Name: "ID_Facultate",
        Value: this.props.ID_Facultate.toString(),
      },
      {
        Name: "ID_Camin",
        Value: "-1"
      }
    ];

    let rap = { ...this.state.RAP_CamereLibereByFacultate };
    rap.loading = true;
    this.setState({ RAP_CamereLibereByFacultate: rap });

    descarcaRaportGeneric("RAP_CamereLibereByFacultate", param)
      .catch(() => {
        alert("A aparut o eroare la descarcarea raportului");
      })
      .finally(() => {
        let rap = { ...this.state.RAP_CamereLibereByFacultate };
        rap.loading = false;
        this.setState({ RAP_CamereLibereByFacultate: rap });
      });
  };

  descarcaRaportCereriRamanereCamin = () => {
    let ID_Facultate = this.props.eSefAdminCamine ? -1 : this.props.ID_Facultate;

    let param = [
      {
        Name: "ID_AnUniv",
        Value: this.props.ID_AnUnivCazare.toString(),
      },
      {
        Name: "ID_Facultate",
        Value: ID_Facultate.toString(),
      },
      {
        Name: "ID_Camin",
        Value: "-1"
      },
      {
        Name: "TipCazareAE",
        Value: "A"
      }
    ];

    let rap = { ...this.state.RAP_CereriRamanereCamin };
    rap.loading = true;
    this.setState({ RAP_CereriRamanereCamin: rap });

    descarcaRaportGeneric("RAP_CereriRamanereCamin", param)
      .catch(() => {
        alert("A aparut o eroare la descarcarea raportului");
      })
      .finally(() => {
        let rap = { ...this.state.RAP_CereriRamanereCamin };
        rap.loading = false;
        this.setState({ RAP_CereriRamanereCamin: rap });
      });
  };

  descarcaRaportListaDeAsteptare = () => {

    let param = [
      {
        Name: "ID_Facultate",
        Value: this.props.ID_Facultate.toString(),
      },
      {
        Name: "ID_TipCiclu",
        Value: "-1"
      },
      {
        Name: "ID_TipFormaInv",
        Value: "-1"
      },
      {
        Name: "ID_Specializare",
        Value: "-1"
      },
      {
        Name: "ID_AnUnivCurent",
        Value: "40"
      },
      {
        Name: "ID_AnUnivCazare",
        Value: this.props.ID_AnUnivCazare.toString()
      },
      {
        Name: "ID_TipCazare",
        Value: "1"
      },
    ];

    let rap = { ...this.state.RAP_ListaDeAsteptareCazare };
    rap.loading = true;
    this.setState({ RAP_ListaDeAsteptareCazare: rap });

    descarcaRaportGeneric("RAP_ListaDeAsteptareCazare", param)
      .catch(() => {
        alert("A aparut o eroare la descarcarea raportului");
      })
      .finally(() => {
        let rap = { ...this.state.RAP_ListaDeAsteptareCazare };
        rap.loading = false;
        this.setState({ RAP_ListaDeAsteptareCazare: rap });
      });
  };

  descarcaRapoortCNFIS() {
    if (!this.state.raportCNFIS.data || !this.state.raportCNFIS.tip) {
      alert("completati toate campurile aferente raportului");
    }

    //todo: raportul de descarcat
  }

  /**
   * Gestioneaza deschidera modalului
   * @method
   */
  handleOpen = () => {
    this.setState({ modalOpen: true });
  };

  /**
   * Gestioneaza inchiderea modalului
   * @method
   */
  handleClose = () =>
    this.setState({
      modalOpen: false,
    });

  /**
   * Gestioneaza schimbarea din dropdown-ul de selectatre a semnului
   * pentru descarcarea raportului de restantieri la taxe
   * @method
   * @param e
   * @param selectieNoua
   */
  raportTaxeSemnChange = (e, selectieNoua) => {
    let raport = { ...this.state.RAP_CaministiRestantieriLaPlata };
    raport.semnSelectat = selectieNoua.value;
    this.setState({ RAP_CaministiRestantieriLaPlata: raport });
  };

  /**
   * Gestioneaza schimbarea din input-ul de selectatre a perioadei de gratie
   * @method
   * @param event
   */
  raportTaxePeriadaGratieChange = (event) => {
    let raport = { ...this.state.RAP_CaministiRestantieriLaPlata };
    raport.perioadaGratie = Math.round(parseInt(event.target.value));

    this.setState({ RAP_CaministiRestantieriLaPlata: raport });
  };

  schimbareDataRaportCNFIS = (date) => {
    let raport = { ...this.state.raportCNFIS };
    raport.data = date;

    this.setState({
      raportCNFIS: raport,
    });
  };

  schimbareTipRaportCNFIS = (e, selectieNoua) => {
    let raport = { ...this.state.raportCNFIS };
    raport.tip = selectieNoua.value;

    this.setState({ raportCNFIS: raport });
  };

  schimbareFocusCalendarCNFIS = ({ focused }) => {
    let raport = { ...this.state.raportCNFIS };
    raport.calendarFocused = focused;

    //this.setState({raportCNFIS: raport})
    // this.setState(() => ({ raportCNFIS: raport }))
    this.setState(() => ({ calendarCNFIS_Focused: focused }));
  };

  render() {
    return (
      <Modal
        size="small"
        trigger={
          <Dropdown.Item
            onClick={this.handleOpen}
            type="button"
            text="Descarcă Rapoarte"
          />
        }
        closeIcon
        open={this.state.modalOpen}
        onClose={this.handleClose}
      >
        <Modal.Header icon="table" content="Rapoarte" />
        <Modal.Content>
          <List divided relaxed="very" verticalAlign="bottom">
            {
              //raportul cu camerele libere in camin
              this.props.RaportOcupareCameraByCaminGen && (
                <List.Item>
                  <List.Content
                    floated="left"
                    style={{ position: "relative", top: "5px" }}
                  >
                    <span>Raport camere libere in camin</span>
                  </List.Content>
                  <List.Content floated="right">
                    <Button
                      color="green"
                      icon
                      labelPosition="right"
                      onClick={this.descarcaRaportCamereLibereCamin}
                      loading={this.state.RaportCamereLibereCamin.loading}
                    >
                      <Icon name="download" />
                      Descarcă
                    </Button>
                  </List.Content>
                </List.Item>
              )
            }

            {/*
              //raportul pe care il afiseaza facultatile la avizier
              this.props.RAP_StudentiCazarePentruAvizier && (
                <List.Item>
                  <List.Content
                    floated="left"
                    style={{ position: "relative", top: "5px" }}
                  >
                    <span>Raport cazări (format pentru avizier)</span>
                  </List.Content>
                  <List.Content floated="right">
                    <Button
                      color="green"
                      icon
                      labelPosition="right"
                      onClick={this.descarcaRaportCazareAvizier}
                      loading={
                        this.state.RAP_StudentiCazarePentruAvizier.loading
                      }
                    >
                      <Icon name="download" />
                      Descarcă
                    </Button>
                  </List.Content>
                </List.Item>
              )
                    */}
            {
              //raportul pe care il afiseaza facultatile la avizier
              this.props.RAP_StudentiCazareCuMedie && (
                <List.Item>
                  <List.Content
                    floated="left"
                    style={{ position: "relative", top: "5px" }}
                  >
                    <span>Raport cazări (cu medie)</span>
                  </List.Content>
                  <List.Content floated="right">
                    <Button
                      color="green"
                      icon
                      labelPosition="right"
                      onClick={this.descarcaRaportStudentiCazareCuMedie}
                      loading={this.state.RAP_StudentiCazareCuMedie.loading}
                    >
                      <Icon name="download" />
                      Descarcă
                    </Button>
                  </List.Content>
                </List.Item>
              )
            }

            {

              this.props.RAP_SituatieOcupareCamine && (
                <List.Item>
                  <List.Content
                    floated="left"
                    style={{ position: "relative", top: "5px" }}
                  >
                    <span>Situatie ocupare camine</span>
                  </List.Content>
                  <List.Content floated="right">
                    <Button
                      color="green"
                      icon
                      labelPosition="right"
                      onClick={this.descarcaRaportSituatieOcupareCamine}
                      loading={this.state.RAP_SituatieOcupareCamine.loading}
                    >
                      <Icon name="download" />
                      Descarcă
                    </Button>
                  </List.Content>
                </List.Item>
              )
            }

            {
              this.props.RAP_StudentiNeconfirmatiByFacultate && (
                <List.Item>
                  <List.Content
                    floated="left"
                    style={{ position: "relative", top: "5px" }}
                  >
                    <span>Raport studenti cu cazare neconfirmata</span>
                  </List.Content>
                  <List.Content floated="right">
                    <Button
                      color="green"
                      icon
                      labelPosition="right"
                      onClick={this.descarcaRaportStudentiNeconfirmatiByFacultate}
                      loading={this.state.RAP_StudentiNeconfirmatiByFacultate.loading}
                    >
                      <Icon name="download" />
                      Descarcă
                    </Button>
                  </List.Content>
                </List.Item>
              )
            }

            {
              this.props.RAP_CamereLibereByFacultate && (
                <List.Item>
                  <List.Content
                    floated="left"
                    style={{ position: "relative", top: "5px" }}
                  >
                    <span>Raport locuri libere facultate</span>
                  </List.Content>
                  <List.Content floated="right">
                    <Button
                      color="green"
                      icon
                      labelPosition="right"
                      onClick={this.descarcaRaportCamereLibereByFacultate}
                      loading={this.state.RAP_CamereLibereByFacultate.loading}
                    >
                      <Icon name="download" />
                      Descarcă
                    </Button>
                  </List.Content>
                </List.Item>
              )
            }

            {
              this.props.RAP_CereriRamanereCamin && (
                <List.Item>
                  <List.Content
                    floated="left"
                    style={{ position: "relative", top: "5px" }}
                  >
                    <span>Raport cereri ramanere camin</span>
                  </List.Content>
                  <List.Content floated="right">
                    <Button
                      color="green"
                      icon
                      labelPosition="right"
                      onClick={this.descarcaRaportCereriRamanereCamin}
                      loading={this.state.RAP_CereriRamanereCamin.loading}
                    >
                      <Icon name="download" />
                      Descarcă
                    </Button>
                  </List.Content>
                </List.Item>
              )
            }
            {/*<List.Item>*/}
            {/*  <List.Content*/}
            {/*    floated="left"*/}
            {/*    style={{ position: "relative", top: "5px" }}*/}
            {/*  >*/}
            {/*    <span>Raport lista de asteptare</span>*/}
            {/*  </List.Content>*/}
            {/*  <List.Content floated="right">*/}
            {/*    <Button*/}
            {/*      color="green"*/}
            {/*      icon*/}
            {/*      labelPosition="right"*/}
            {/*      onClick={this.descarcaRaportListaDeAsteptare}*/}
            {/*      loading={this.state.RAP_ListaDeAsteptareCazare.loading}*/}
            {/*    >*/}
            {/*      <Icon name="download" />*/}
            {/*      Descarcă*/}
            {/*    </Button>*/}
            {/*  </List.Content>*/}
            {/*</List.Item>*/}
            {/*<List.Item>*/}
            {/*  <List.Content floated='left' style={{position: 'relative', top: '5px'}}>*/}
            {/*    <span>Raport restantieri taxe cu un numar{' '}*/}
            {/*      <Dropdown*/}
            {/*        inline*/}
            {/*        placeholder="completeaza semn"*/}
            {/*        search*/}
            {/*        options={this.state.RAP_CaministiRestantieriLaPlata.semnePosibile}*/}
            {/*        onChange={this.raportTaxeSemnChange}*/}
            {/*      />*/}
            {/*      {' '}*/}
            {/*      <input*/}
            {/*        type="number"*/}
            {/*        min={1}*/}
            {/*        step={1}*/}
            {/*        style={{width: "50px"}}*/}
            {/*        onChange={this.raportTaxePeriadaGratieChange}*/}
            {/*        value={this.state.RAP_CaministiRestantieriLaPlata.perioadaGratie}*/}
            {/*      />*/}
            {/*      {'  zile restante'}*/}
            {/*    </span>*/}
            {/*  </List.Content>*/}
            {/*  <List.Content floated='right'>*/}
            {/*    <Button*/}
            {/*      color='green'*/}
            {/*      icon labelPosition='right'*/}
            {/*      disabled={!this.state.RAP_CaministiRestantieriLaPlata.perioadaGratie || !this.state.RAP_CaministiRestantieriLaPlata.semnSelectat}*/}
            {/*      onClick={this.descarcaRaportRestantieriCamin}*/}
            {/*      loading={this.state.RAP_CaministiRestantieriLaPlata.loading}*/}
            {/*    >*/}
            {/*      <Icon name='download'/>*/}
            {/*      Descarcă*/}
            {/*    </Button>*/}
            {/*  </List.Content>*/}
            {/*</List.Item>*/}

            {this.props.RapoortCNFIS && (
              <List.Item>
                <List.Content
                  floated="left"
                  style={{ position: "relative", top: "5px" }}
                >
                  <span>
                    Raport CNFIS{" "}
                    <Dropdown
                      placeholder="alege numeric/nominal"
                      inline
                      search
                      options={[
                        { key: "numeric", value: "numeric", text: "numeric" },
                        { key: "nominal", value: "nominal", text: "nominal" },
                      ]}
                      onChange={this.schimbareTipRaportCNFIS}
                    />
                  </span>
                </List.Content>
                <List.Content floated="right">
                  <SingleDatePicker
                    date={this.state.raportCNFIS.data}
                    onDateChange={this.schimbareDataRaportCNFIS}
                    displayFormat="DD/MM/YYYY"
                    placeholder="Data"
                    focused={this.state.calendarCNFIS_Focused}
                    onFocusChange={this.schimbareFocusCalendarCNFIS}
                    numberOfMonths={1}
                    isOutsideRange={() => false}
                  />
                  <Button
                    color="green"
                    icon
                    labelPosition="right"
                    disabled={
                      !this.state.raportCNFIS.tip ||
                      !this.state.raportCNFIS.data
                    }
                    onClick={this.descarcaRapoortCNFIS}
                    style={{ marginLeft: 25 }}
                  >
                    <Icon name="download" />
                    Descarcă
                  </Button>
                </List.Content>
              </List.Item>
            )}
          </List>
        </Modal.Content>
        <Modal.Actions>
          <Button basic color="red" onClick={this.handleClose} type="button">
            <Icon name="log out" />
            Ieșire
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

MeniuRapoarte.propTypes = {
  RaportOcupareCameraByCaminGen: PropTypes.bool,
  RapoortCNFIS: PropTypes.bool,
  ID_Camin: PropTypes.number,
  RAP_StudentiCazarePentruAvizier: PropTypes.bool,
  RAP_SituatieOcupareCamine: PropTypes.bool,
  RAP_StudentiNeconfirmatiByFacultate: PropTypes.bool,
  RAP_CamereLibereByFacultate: PropTypes.bool,
  eSefAdminCamine: PropTypes.bool,

  //impune sa se paseze props-urile care sunt necesare pentru rapoartele care au fost selectate prin props
  ID_Facultate: function (props, propName, componentName) {
    if (
      props["RAP_StudentiCazarePentruAvizier"] === true &&
      (props[propName] === undefined || typeof props[propName] != "number")
    ) {
      return new Error(
        "Pentru raportul RAP_StudentiCazarePentruAvizier este necesar ID-ul facultatii "
      );
    }
  },
  ID_AnUnivCurent: function (props, propName, componentName) {
    if (
      props["RAP_StudentiCazarePentruAvizier"] === true &&
      (props[propName] === undefined || typeof props[propName] != "number")
    ) {
      return new Error(
        "Pentru raportul RAP_StudentiCazarePentruAvizier este necesar ID-ul anului curent "
      );
    }
  },
  ID_AnUnivCazare: function (props, propName, componentName) {
    if (
      props["RAP_StudentiCazarePentruAvizier"] === true &&
      (props[propName] === undefined || typeof props[propName] != "number")
    ) {
      return new Error(
        "Pentru raportul RAP_StudentiCazarePentruAvizier este necesar ID-ul anului de cazare "
      );
    }
  },
};

MeniuRapoarte.defaultProps = {
  RaportOcupareCameraByCaminGen: false,
  RapoortCNFIS: false,
  RAP_StudentiCazarePentruAvizier: false,
  RAP_StudentiCazareCuMedie: false,
  RAP_SituatieOcupareCamine: false,
  RAP_StudentiNeconfirmatiByFacultate: false,
  RAP_CamereLibereByFacultate: false,
};

export default MeniuRapoarte;
