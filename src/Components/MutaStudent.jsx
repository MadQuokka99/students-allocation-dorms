import {Dropdown, Modal, Button, Grid, Header} from "semantic-ui-react";
import React, {Component} from "react";
import axiosRef from "../axios-AGSIS-API-refactor";

/**
 * componenta afiseaza un Modal care permite utilizatorului sa aleaga caminul si camera in care doreste sa mute studentul selectat
 */
class MutaStudent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      modalMutareOpen: false,

      modalAtentionareOpen: false,

      idCaminSelectat: null,
      listaCamereCamin: [],

      idCameraSelectata: null

    }
  }

  handleOpenModalMutare = () => {
    this.setState({
      modalMutareOpen: true
    })
  }
  handleCloseModalMutare = () => {
    this.setState({
      modalMutareOpen: false,
      idCaminSelectat: null,
      idCameraSelectata: null
    })
  }

  handleOpenModalAtentie = () => {
    this.setState({
      modalAtentionareOpen: true
    })
  }

  handleCloseModalAtentie = () => {
    this.setState({
      modalAtentionareOpen: false,
      idCaminSelectat: null,
      idCameraSelectata: null
    })
  }

  /**
   *
   * @param camin
   * functia returneaza lista camerelor din caminul selectat
   */
  selecteazaCamin = (camin) => {
    this.setState({
      idCaminSelectat: camin.value,
      idCameraSelectata: null
    })

    let idFacultate = this.props.idFacultate;

    if (this.props.eSefAdminCamine) {
      idFacultate = -1;
    }
    /**
     * se preia lista de camere din caminul respectiv
     */
    axiosRef
      .get(
        `FacultateCamera/FacultateCameraListByFacultateAnUnivCamin?ID_Facultate=${idFacultate}&ID_AnUniv=${this.props.idAnCazare}&ID_Camin=${camin.value}`
      )
      .then((response) => {
        let listaCamereRef = response.data;
        let camereCamineRef = [];

        //console.log(response.data);

        listaCamereRef.forEach((cam, index) => {
          let camera = {
            key: index,
            text: "Camera " + cam.NumarCamera,
            value: cam.ID_Camera,
            disabled: this.props.eSefAdminCamine ? (cam.NrLocuri - cam.NrStudentiCazati) <= 0 : (cam.NrLocuriAlocateFacultate - cam.NrStudentiCazati) <= 0
          }
          camereCamineRef.push(camera)
        })
        this.setState({
          listaCamereCamin: camereCamineRef
        })

        //console.log(camereCamineRef)
      })
  }
  /**
   * modalul apare atunci cand utilizatorul apasa pe butonul 'Mută student' si are utilitatea de a confirma mutarea studentului
   */
  modalAtentionare = () => {
    return (
      <Modal
        closeIcon
        size="small"
        open={this.state.modalAtentionareOpen}
        onClose={this.handleCloseModalMutare}
      >
        <Header content="Mutare student" icon="arrow alternate circle right outline"/>
        <Modal.Content>
          <p>
            Doriți mutarea studentului în camera selectată?
          </p>
        </Modal.Content>
        <Modal.Actions>
          <Button type="button" basic color='green' onClick={() => this.mutaStudent()}>
            Da
          </Button>
          <Button type="button" basic color='red' onClick={() => this.handleCloseModalAtentie()}>
            Nu
          </Button>
        </Modal.Actions>
      </Modal>
    )
  }


  selecteazaCamera = (camera) => {

    this.setState({
      idCameraSelectata: camera.value,
    })
  }

  /**
   * metoda muta studentul selectat in camera aleasa
   */
  mutaStudent = () => {
    axiosRef
      .post(`Cazare/CazareMutaStudent?ID_Cazare=${this.props.student.ID_Cazare}&ID_CameraDestinatie=${this.state.idCameraSelectata}`)
      .then(response => {

        console.log(this.props.student)

        const ID_CameraActuala = this.props.student.CazatInID_Camera;
        let studentUpdated = {...this.props.student};
        studentUpdated.CazatInID_Camera = this.state.idCameraSelectata;
        studentUpdated.ID_Cazare = response.data.ID_Cazare;

        console.log(studentUpdated)
        this.setState({
          idCameraSelectata: null,
          idCaminSelectat: null,
          modalAtentionareOpen: false,
          modalMutareOpen: false

        }, () => this.props.mutaStudent(studentUpdated, ID_CameraActuala, this.state.idCameraSelectata));

        //console.log(response);
      })
      .catch(e => {
        alert(e.response.data)
        console.log(e);
      })
  }

  render() {
    return (
      <Modal
        className={"overflow-visible"}
        closeIcon
        size="small"
        trigger={
          <Dropdown.Item
            onClick={this.handleOpenModalMutare}
            type="button"
            text="Mută student"
          />
        }
        open={this.state.modalMutareOpen}
        onClose={this.handleCloseModalMutare}
      >
        <Header content="Alegeți căminul și camera dorite" icon="arrow alternate circle right outline"/>
        <Modal.Content>
          <Modal.Description>
            <Grid centered style={{
              paddingLeft: "5px"
            }}>
              <Grid.Row columns="equal">
                <Grid.Column
                  className="centrare"
                >
                  <Dropdown
                    className="bannerDropdownMutaStudent"
                    button
                    search
                    selection
                    value={this.state.idCaminSelectat}
                    onChange={(e, camin) => this.selecteazaCamin(camin)}
                    options={this.props.listaCamine}
                    placeholder="Cămin"
                  />
                </Grid.Column>

                <Grid.Column className="centrare">
                  <Dropdown
                    disabled={this.state.idCaminSelectat === null}
                    className="bannerDropdownMutaStudent"
                    button
                    search
                    selection
                    value={this.state.idCameraSelectata}
                    onChange={(e, camera) => this.selecteazaCamera(camera)}
                    options={this.state.listaCamereCamin}
                    placeholder="Camera"
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions style={{
          paddingTop: "15px"
        }}>
          <Button type="button" disabled={this.state.idCameraSelectata === null} basic color='green'
                  onClick={() => this.handleOpenModalAtentie()}>
            Mută student
          </Button>
          <Button type="button" basic color='red' onClick={() => this.handleCloseModalMutare()}>
            Închide
          </Button>
        </Modal.Actions>
        {
          this.modalAtentionare()
        }
      </Modal>

    )
  }
}

export default MutaStudent;



