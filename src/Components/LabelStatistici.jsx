import React, { Component } from "react";
import { Label, Icon } from "semantic-ui-react";
import "./LabelStatistici.css";

class LabelStatistici extends Component {
    render() {
        return (
            <Label className="labelStyle">
                <Icon
                    name={this.props.icon}
                    style={{ color: this.props.culoareFacultate }}
                />
                {this.props.title}
                <Label.Detail>{this.props.detail}</Label.Detail>
            </Label>
        );
    }
}

export default LabelStatistici;
