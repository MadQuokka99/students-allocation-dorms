/**
 * @description
 * functia grupeaza un array dupa o proprietate data
 * @param {*} array array-ul de grupat
 * @param {*} property proprietatea dupa care se va face gruparea
 * @returns returneaza un array de grupuri (array bidimensional)
 */

export const groupBy = (array, property) => {
  //array.reduce reurns an object that has the groups as props
  const groupedObject = array.reduce((formedGroup, currentObject) => {
    const key = currentObject[property];
    if (!formedGroup[key]) {
      formedGroup[key] = [];
    }
    formedGroup[key].push(currentObject);
    return formedGroup;
  }, {});

  //transform the object to an iterable array
  let groupedArray = [];
  for (const group in groupedObject) {
    groupedArray.push(groupedObject[group]);
  }

  //returns array of groups(array)
  return groupedArray;
};
