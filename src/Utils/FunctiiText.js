
/**
 * Functie care permite cautarea unui student dupa nume si ignora diacriticele
 * Facuta sa inlocuiasca functia standard de cautare din Semantic UI
 * @method
 * @param filteredOptions Optiunile filtrate pana acum
 * @param searchQuery Literele introduse de utilizator dupa care se va face cautarea
 * @returns lista de obiecte care se potrivesc cu ce s-a cautat
 *
 * @example <caption> Pentru un dropdown din Semantic UI </caption>
 * <Dropdown search={functieCautareNume}/>
 */
export const functieCautareNume = (filteredOptions, searchQuery) => {
  return filteredOptions?.filter(el =>
    functieCautareTextStr(el.text, searchQuery)
  );
};

export const normalizeString = (str) => {
  return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase()
}

/**
 *  cauta in primul string dat ca paramatru toate cuvintele din cel ce-al doilea string dat ca paramatru. Se realizeaza normalizarea ambelor stringuri
 * @param stringToSearchIn
 * @param searchQuery
 * @return {boolean}
 */
export const functieCautareTextStr = (stringToSearchIn, searchQuery) => {
  if (searchQuery == null) {
    return true;
  }

  let processedSearchQuery = normalizeString(searchQuery.trim());

  if (processedSearchQuery === '') {
    return true;
  }

  let searchTokens = processedSearchQuery.split(" ");

  let processedStringToSearchIn = normalizeString(stringToSearchIn);

  let searchQueryMatched = true;

  for (let i = 0; i < searchTokens.length; i++) {
    const token = searchTokens[i];
    const matches = processedStringToSearchIn.includes(token);
    if (!matches) {
      searchQueryMatched = false;
      break;
    }
  }

  return searchQueryMatched;
}
