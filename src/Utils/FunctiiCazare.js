import axiosRef from "../axios-AGSIS-API-refactor";
import { toast } from "react-semantic-toasts";

/**
 *
 * @param student
 * @param idcamera
 * @param dataDeCand
 * @param dataPanaCand
 * @param idAnUniv
 * @param idFacultate
 * @returns {Promise<AxiosResponse<any>>}
 *
 * functia de cazare a unui student intr-o camera
 */
export const cazeazaStudent = async (
  student,
  idcamera,
  dataDeCand,
  dataPanaCand,
  idAnUniv,
  idFacultate,
  idTipCazare
) => {
    //console.log(student);
  return await axiosRef
    .post("Cazare/CazareAdd", {
      ID_Student: student.ID_Student,
      ID_Camera: idcamera,
      DataDeCand: dataDeCand,
      DataPanaCand: dataPanaCand,
      ID_AnUniv: idAnUniv,
      Status: 1,
      CazareConfirmata: false,
      ID_CazareAnterioara: student.ID_UltimaCazare,
      ID_MotivScutiri: student.ID_MotivScutiri,
      ID_Facultate: idFacultate,
      ID_TipCazare: idTipCazare
    })
    .then((response) => {
      //console.log(response);
      //console.log(response.config.data);
      //console.log(response.data);

      /**
       * toast de informare cazare reusita
       */
      toast({
        type: "success",
        icon: "check",
        title: "Cazarea a fost făcută cu succes!",
        time: 4000,
      });

      /**
       * toast de informare in legatura cu trimiterea de mail
       */

      if(response.data.MailTrimis)
      toast({
        type: "success",
        icon: "check",
        title: response.data.ResponseServerTrimitereMail,
        time: 4000,
      });

      if(response.data.EroareTrimitereMail)
        toast({
          type: "error",
          icon: "x",
          title: response.data.ResponseServerTrimitereMail,
          time: 4000,
        });

      return response.data;
    })
    .catch((error) => {
      console.log(error.response);
      // console.log(error.response.data);
      // console.log(error.response.status);

      alert(error.response.data);

      throw Error(error.response.status);
    });
};
