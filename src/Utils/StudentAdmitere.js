export const eStudentAdmitere = (student, idAnCazare) => {
  if (student.ID_AnUniv === idAnCazare && student.DenumireAnStudiu === "I")
    return true;

  if (
    student.ID_AnUniv !== idAnCazare ||
    (student.ID_AnUniv === idAnCazare && student.DenumireAnStudiu !== "I")
  )
    return false;
};
