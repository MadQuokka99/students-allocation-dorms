import axiosRef from './axios-AGSIS-API-refactor';

/**
 * Functie care permite cautarea unui student dupa nume si ignora diacriticele
 * Facuta sa inlocuiasca functia standard de cautare din Semantic UI
 * @method
 *
 * @example <caption> Exemplu de request valid </caption>
 * const numeRaport = ""
 * const parametrii = [
 *    {
 *
 *    },
 *
 * ]
 *
 */
export  const descarcaRaportGeneric = async (numeRaport, parametrii) => {
  //se formeaza obiectul de trimis dupa cum il asteapta serverul
  let obj = {
    NumeRaport: numeRaport,
    param: parametrii
  };

  console.log(JSON.stringify(obj))

  return axiosRef
    .post(
      'Cazare/RSRapoarte',
      obj,
      {headers: {'Content-Type': 'application/json'}, responseType: 'blob'}
      )
    .then((response) => {

      //se ia numele raportului din headerele trimise de pe server
      let filename = "";
      let disposition = response.headers["content-disposition"]
      if (disposition && disposition.indexOf('attachment') !== -1) {
        let filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        let matches = filenameRegex.exec(disposition);
        if (matches != null && matches[1]) {
          filename = matches[1].replace(/['"]/g, '');
        }
      }

      if(!filename){
        filename = "Raport.xls"
      }

      //se creeaza un element si simuleaza un click pe ele pentru a face descarcarea fisierului in browser
      const downloadUrl = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = downloadUrl;
      link.setAttribute('download', filename); //any other extension
      document.body.appendChild(link);
      link.click();
      link.remove();
    })
};


