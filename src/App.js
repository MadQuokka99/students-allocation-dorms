import React, { Component } from "react";

import Facultati from "./Components/Facultate/Facultati";
import axios from "./axios-AGSIS-API";
import Student from "./Components/Decanat/Student";
import { Table } from "semantic-ui-react";

class App extends Component {
  state = {
    facultati: []
  };

  componentDidMount() {
    axios
      .get("/Facultate/FacultateList")
      .then(response => response.data)
      .then(data => {
        this.setState({ facultati: data })
      })
        }



  render() {
    return (
      <div>
        <Table collapsing>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Denumire Facultate</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            <Facultati facultati={this.state.facultati} />
          </Table.Body>
        </Table>
      </div>
    )
  }
}

export default App;
