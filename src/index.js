import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import "semantic-ui-css/semantic.min.css";
import * as serviceWorker from "./serviceWorker";
import PaginaDecanat from "./pages/Decanat/Decanat";

ReactDOM.render(<PaginaDecanat />, document.getElementById("root"));
serviceWorker.unregister();
