import React, {createRef, Component} from "react";
import moment from "moment";

import axiosRef from "../../axios-AGSIS-API-refactor";
import {getUsername} from "../../GetUsername";
import {getCookie, setCookie} from "../../Utils/CookieHandler";
import ListaStudenti from "../../Components/Decanat/ListaStudenti/ListaStudenti";
import ListaCamere from "../../Components/Decanat/roomList/ListaCamere";
import "./Decanat.css";
import LabelStatistici from "../../Components/LabelStatistici";

import data from "../../data.json";

import {
  Container,
  Header,
  Segment,
  Grid,
  Loader,
  Icon,
  Dropdown, Checkbox
} from "semantic-ui-react";
import MeniuRapoarte from "../../Components/MeniuRapoarte";
import Menu from "semantic-ui-react/dist/commonjs/collections/Menu";
import {functieCautareNume} from "../../Utils/FunctiiText";

/**
 * retinerea culorilor facultatilor
 */
var bannerColors = {
  default: "#771d82",
  mecanica: "#f7ae20",
  itmi: "#007181",
  sim: "#0b9b8b",
  iesc: "#0064a0",
  silvic: "#19792d",
  il: "#8a3d16",
  mi: "#003c7e",
  seaa: "#1da63f",
  medicina: "#e71e26",
  litere: "#ed7324",
  sport: "#288aba",
  drept: "#771d82",
  constructii: "#aa0533",
  muzica: "#bb0084",
  psihoedu: "#ed7d72",
  at: "#4c2380",
  socio: "#dc0063",
  dpm: "#8cbe42",
};

/**
 * retinerea id-urilor facultatilor
 */

const MECANICA = 6; //Inginerie Mecanica
const ITMI = 8; // Inginerie tehnologică și management industrial
const SIM = 9; //STIINTA SI INGINERIA MATERIALELOR
const IESC = 10; //inginerie electrica si stiinta calculatoarelor
const SILVIC = 11; //Silvicultură și exploatări forestiere
const IL = 12; //Ingineria Lemnului
const MI = 13; //Mate Info
const SEAA = 14; //Științe economice și administrarea afacerilor
const MEDICINA = 16; //medicina
const LITERE = 17; //litere
const SPORT = 18; //Educație Fizică și Sporturi Montane
const DREPT = 19; //Drept
const CONSTRUCTII = 20; //Constructii
const MUZICA = 28; //Muzica
const PSIHOEDU = 37; //Psihologie şi Ştiințele Educației
const AT = 45; //Alimentatie si Turism
const SOCIO = 60; //Sociologie si Comunicare
const DPM = 61; //Design si Produs de meniu

//se citeste calea ghidului de utilizare a paginii de Decanat dintr-un fisier json
const pathGhidUtilizare = data.filePathButon;

class PaginaDecanat extends Component {
  contextRef = createRef();

  constructor(props) {
    super(props);
    this.state = {
      prodecan: null,
      eSefAdminCamine: false,

      listaFacultati: [],
      idFacultate: null,
      culoareFacultate: null,
      denumireFacultate: null,

      idAnUniversitarCurent: null,
      anUniversitarCurent: null,
      //data de cand cazarile efectuate sunt valabile
      cazareDataDeCand: null,
      //data pana cand cazarile efectuate sunt valabile
      cazareDataPanaCand: null,

      idAnCazare: null,
      anUniversitarPrecazare: null,
      etapeCazare: [],
      nrCereriPrecazare: 0,

      nrTotalStudenti: 0,
      listaStudenti: [],

      listaCamine: [],

      //pentru actualizarea labels raport cazare
      nrLocuriTotal: 0,
      nrLocuriAlocate: 0,
      nrLocuriOcupate: 0,
      nrLocuriLibereFete: 0,
      nrLocuriLibereBaieti: 0,
      nrLocuriLibereNespecificat: 0,

      tipCameraMFCazare: "N",
      ID_CameraCazare: null,
      raportCazareCamin: null,

      ID_TipCazare: 1,
    };

    //referinta pentru a putea apela o functie din copil in parinte
    this.refChild = React.createRef();
  }

  /**
   * determina culoarea specifica facultatii pe baza id-ului, se trimite mai departe in componente prin props
   */

  getCuloareFacultate = (id) => {
    if (id === MUZICA) return bannerColors.muzica;
    else if (id === SEAA) return bannerColors.seaa;
    else if (id === MI) return bannerColors.mi;
    else if (id === MECANICA) return bannerColors.mecanica;
    else if (id === SOCIO) return bannerColors.socio;
    else if (id === CONSTRUCTII) return bannerColors.constructii;
    else if (id === SIM) return bannerColors.sim;
    else if (id === DREPT) return bannerColors.drept;
    else if (id === DPM) return bannerColors.dpm;
    else if (id === AT) return bannerColors.at;
    else if (id === ITMI) return bannerColors.itmi;
    else if (id === SPORT) return bannerColors.sport;
    else if (id === SILVIC) return bannerColors.silvic;
    else if (id === IESC) return bannerColors.iesc;
    else if (id === IL) return bannerColors.il;
    else if (id === LITERE) return bannerColors.litere;
    else if (id === MEDICINA) return bannerColors.medicina;
    else if (id === PSIHOEDU) return bannerColors.psihoedu;
    else return bannerColors.default;
  };

  getCazareInfo = (idFacultate, eSefAdminCamine, ID_TipCazare) => {
    this.setState({loadedStudenti: false})

    axiosRef
      .get(
        `Cazare/InfoNecesareCazareStudenti?ID_Facultate=${idFacultate}&ID_TipCAzare=${this.state.ID_TipCazare}&esteSefAdminCamine=${eSefAdminCamine}`
      )
      .then((response) => {
        //console.log(response.data);

        let cazareDataDeCand;
        let cazareDataPanaCand;
        //daca e selectata cazarea de vara SI datele sunt definite
        if (ID_TipCazare === 2 && response.data.CaminePerioadaCazareVara.DataInceput && response.data.CaminePerioadaCazareVara.DataSfarsit) {
          cazareDataDeCand = response.data.CaminePerioadaCazareVara.DataInceput;
          cazareDataPanaCand = response.data.CaminePerioadaCazareVara.DataSfarsit
        } else {
          cazareDataDeCand = response.data.AnUniversitarPrecazare.DataInceputAnUniv;
          cazareDataPanaCand = response.data.AnUniversitarPrecazare.DataSfirsitAnUniv
        }

        this.setState({
          idAnCazare: response.data.AnUniversitarPrecazare.ID_AnUniv,
          anUniversitarPrecazare: response.data.AnUniversitarPrecazare,
          facultate: response.data.Facultate.Denumire,
          cazareDataDeCand: cazareDataDeCand,
          cazareDataPanaCand: cazareDataPanaCand,
          culoareFacultate: this.state.eSefAdminCamine
            ? this.getCuloareFacultate(-1)
            : this.getCuloareFacultate(idFacultate),
        });

        for (let i = 0; i < response.data.CereriPrecazare.length; i++) {
          try {
            let comentarii = JSON.parse(response.data.CereriPrecazare[i].ComentariiCamine)
            response.data.CereriPrecazare[i].ComentariiCamine = comentarii.filter(x => x.tipcomentariu !== "automat");
          } catch {
            response.data.CereriPrecazare[i].ComentariiCamine = []
          }
        }

        let listaStudentiCompleta = response.data.CereriPrecazare;

        let listaCereriPrecazare = response.data.CereriPrecazare.filter(
          (student) =>
            student.ID_CererePrecazare !== -1 &&
            student.CazareConfirmata === false
        );
        //console.log("Cereri precazare:");
        //console.log(listaCereriPrecazare);

        this.setState({
          nrTotalStudenti: listaStudentiCompleta.length,
          nrCereriPrecazare: listaCereriPrecazare.length,
          listaStudenti: listaStudentiCompleta,
          loadedStudenti: true,
        });

        let etapeCazare = [];

        let etapa1 = [];
        etapa1.push(new Date(response.data.CamineDateFaza1.DataInceput));
        etapa1.push(new Date(response.data.CamineDateFaza1.DataSfarsit));
        etapeCazare.push(etapa1);

        let etapa2 = [];
        etapa2.push(new Date(response.data.CamineDateFaza2.DataInceput));
        etapa2.push(new Date(response.data.CamineDateFaza2.DataSfarsit));
        etapeCazare.push(etapa2);

        this.setState({etapeCazare: etapeCazare});

        /**
         * preia lista de camine si etajele corespunzatoare alocate facultatii respective
         */
        let listaCamineRef = response.data.CamineFacultate;
        let camine = [];

        listaCamineRef.forEach((camin, index) => {
          let camera = {
            key: index,
            text: camin.DenumireCamin,
            value: camin.ID_Camin,
            etaje: camin.Etaje,
          };
          camine.push(camera);
        });

        this.setState({
          listaCamine: camine,
          loadedCamine: true,
        });
        //console.log(this.state.listaCamine); // afiseaza lista caminelor alocate facultatii

        /**
         * preia informatiile statistice cu privire la cazarile in camine
         */
        axiosRef
          .get(`Cazare/CazareRaportLocuri?ID_AnUniv=${response.data.AnUniversitarPrecazare.ID_AnUniv}&ID_Facultate=${idFacultate}&ID_Camin=-1`)
          .then((resp) => {
            let statisticiCazare = resp.data;

            this.setState({
              nrLocuriAlocate: statisticiCazare.NrTotalLocuriAlocate,
              nrLocuriOcupate: statisticiCazare.NrStudentiCazati,
              nrLocuriLibereFete: statisticiCazare.NrLocuriLibereFete,
              nrLocuriLibereBaieti: statisticiCazare.NrLocuriLibereBaieti,
              nrLocuriLibereNespecificat:
              statisticiCazare.NrLocuriLibereGenNespecificat,
            });
          })

      })
      .catch((error) => {
        console.log(error);
        console.log(error.response);
      });
  };

  componentDidMount() {
    /**
     * este retinut username-ul decanului logat
     */
    const username = getUsername();

    // se ia setarea despre caazrea pe vara din cookie-uri, daca exista
    const cookieID_TipCazare = getCookie("ID_TipCazare");
    const ID_TipCazare = cookieID_TipCazare != null ? parseInt(cookieID_TipCazare) : 1

    this.setState({
      prodecan: username,
      ID_TipCazare: ID_TipCazare
    });
    /**
     * se preiau informatiile despre anul universitar curent
     */
    axiosRef
      .get("AnUniversitar/AnUniversitarCurent")
      .then((anUniversitar) => {
        this.setState({
          anUniversitarCurent: anUniversitar.data,
          idAnUniversitarCurent: anUniversitar.data.ID_AnUniv,
        });

        axiosRef
          .get(
            `Cazare/RaportCazareCamine?ID_AnUniv=${anUniversitar.data.ID_AnUniv}`
          )
          .then((r) => {
            this.setState({
              raportCazareCamin: r.data
            });
          });

        /**
         * se identifica rolul utilizatorului pe baza username-ului, respectiv daca eSefaAdminCamine=true sau false
         */
        axiosRef
          .get(`Users/IsInRole?role=SefAdminCamine&username=${username}`)
          .then((eSefAdminCamine) => {
            if (eSefAdminCamine.data || username === "andrei.curta@student.unitbv.ro") {
              this.setState({eSefAdminCamine: eSefAdminCamine.data});

              /**
               * se retine lista de facultati pentru dropdown-ul de alegere a facultatii pentru eSefAdminCamine=true
               */
              axiosRef.get("Facultate/FacultateList").then((facultati) => {
                let tempFacultati = [];

                // se filtreaza facultatile fictive(cele care nu au tip facultate = F), dar lasam departamentul doctorat, de unde se mai cazeaza studenti
                facultati.data
                  .filter(x => x.TipFacultate === "F" || x.ID_Facultate === 62)
                  .forEach((facultate) => {
                    let fac = {
                      text: facultate.Denumire.toUpperCase(),
                      value: facultate.ID_Facultate,
                    };
                    tempFacultati.push(fac);
                  });

                this.setState({
                  listaFacultati: tempFacultati,
                  idFacultate: tempFacultati[0].value,
                  denumireFacultate: tempFacultati[0].text,
                  culoareFacultate: this.getCuloareFacultate(-1),
                });

                this.getCazareInfo(
                  tempFacultati[0].value,
                  eSefAdminCamine.data,
                  ID_TipCazare
                );
              });
            } else {
              /**
               * daca eSefAdminCamine=false, se preiau datele despre facultatea la care utilizatorul este profesor
               *
               * daca lista de facultati returnata are doar 1 element, se afiseaza obisnuit
               * daca lista are mai mult de 1 element, apare un dropdown de alegere a facultatii
               */
              axiosRef
                .get(
                  `Facultate/FacultateListByUsernameAnUniv?username=${username}&ID_AnUniv=${anUniversitar.data.ID_AnUniv}`
                )
                .then((facultati) => {
                  //console.log(facultati.data);

                  let tempFacultati = [];

                  if (facultati.data.length > 1) {
                    facultati.data.forEach((facultate) => {
                      let fac = {
                        text: facultate.Denumire.toUpperCase(),
                        value: facultate.ID_Facultate,
                      };
                      tempFacultati.push(fac);
                    });

                    this.setState({
                      listaFacultati: tempFacultati,
                      idFacultate: tempFacultati[0].value,
                      culoareFacultate: this.getCuloareFacultate(
                        tempFacultati[0].value
                      ),
                      denumireFacultate: tempFacultati[0].text,
                    });

                    this.getCazareInfo(
                      tempFacultati[0].value,
                      eSefAdminCamine.data,
                      ID_TipCazare
                    );
                  } else {
                    let idFacultate = facultati.data[0].ID_Facultate;
                    let culoareFacultate = this.getCuloareFacultate(
                      idFacultate
                    );

                    this.setState({
                      idFacultate: idFacultate,
                      culoareFacultate: culoareFacultate,
                      denumireFacultate: facultati.data[0].Denumire,
                    });

                    this.getCazareInfo(idFacultate, eSefAdminCamine.data, ID_TipCazare);
                  }
                });
            }
          });
      })
      .catch((e) => console.log(e.response));
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.idFacultate !== this.state.idFacultate &&
      prevState.idFacultate !== null
    ) {
      this.getCazareInfo(this.state.idFacultate, this.state.eSefAdminCamine, this.state.ID_TipCazare);
    }

  }

  /**
   * Determina rerandarea pentru o noua facultate selectata
   * @param {*} e event
   * @param {*} facultate facultatea selectata
   */
  handleSelecteazaFacultate = (e, facultate) => {
    this.setState({
      idFacultate: facultate.value,
      loadedStudenti: false,
      loadedCamine: false,
    });
  };

  /**
   * metoda returneaza studentul din lista de studenti care au facut precazare al carui id corespunde cu id-ul cautat
   */
  returneazaStudent = (studentID) => {
    let student = this.state.listaStudenti.find((element) => {
      return element.ID_Student.toString() === studentID.toString();
    });
    return student;
  };

  /**
   *
   * @param student
   * @returns {*}
   *
   * studentul primit ca parametru este sters din lista de studenti care au facut cerere de precazare (din partea stanga)
   * si este adaugat in lista de studenti cazati (din partea dreapta)
   */

  schimbaStatusStudent = (student, indiceCameraSauStudent) => {
    let listaStudenti = [...this.state.listaStudenti];
    for (let i in listaStudenti) {
      if (listaStudenti[i].ID_Student === student.ID_Student) {
        listaStudenti.splice(i, 1);
        break;
      }
    }

    this.setState({
      listaStudenti: listaStudenti,
    });

    /**
     * functia schimbaStatusStudent este apelata din parinte in copil pentru a actualiza lista de studenti cazati in camera specificata
     */
    if (indiceCameraSauStudent === 1) {
      this.refChild.current.schimbaStatusStudent(student, 1);
    }

    /**
     * functia selecteazaCamin este apelata din parinte in copil pentru a se deschide camera studentului care a fost cazat prin apasarea butonului "Repeta cazarea anterioara"
     */
    if (indiceCameraSauStudent === 1) {
      this.refChild.current.selecteazaCamin(
        student.ID_CaminUltimaCazare,
        student.EtajUltimaCazare,
        student.NumarCameraUltimaCazare
      );
    }

    return student;
  };

  /**
   * determina sigla facultatii pe baza id-ului, pentru un id incorect, se va afisa mereu UT
   */
  // getSiglaFacultate = () => {
  //     if (this.state.idFacultate === MUZICA) return "Muzica";
  //     else if (this.state.idFacultate === SEAA) return "SEAA";
  //     else if (this.state.idFacultate === MI) return "MI";
  //     else if (this.state.idFacultate === MECANICA) return "IM";
  //     else if (this.state.idFacultate === SOCIO) return "SC";
  //     else if (this.state.idFacultate === CONSTRUCTII) return "Constructii";
  //     else if (this.state.idFacultate === SIM) return "SIM";
  //     else if (this.state.idFacultate === DREPT) return "Drept";
  //     else if (this.state.idFacultate === DPM) return "DPM";
  //     else if (this.state.idFacultate === AT) return "AT";
  //     else if (this.state.idFacultate === ITMI) return "ITMI";
  //     else if (this.state.idFacultate === SPORT) return "EFSM";
  //     else if (this.state.idFacultate === SILVIC) return "Silvicultura";
  //     else if (this.state.idFacultate === IESC) return "IESC";
  //     else if (this.state.idFacultate === IL) return "IL";
  //     else if (this.state.idFacultate === LITERE) return "Litere";
  //     else if (this.state.idFacultate === MEDICINA) return "Medicina";
  //     else if (this.state.idFacultate === PSIHOEDU) return "PSE";
  //     else return "UT";
  // };

  /**
   * afiseaza un loader de asteptare pana cand se extrag informatiile din baza de date
   */
  loadingView() {
    return (
      <Container textAlign="center">
        <Segment>
          <Loader active inline size="large">
            Vă rugăm așteptați, se încarcă datele.
          </Loader>
        </Segment>
      </Container>
    );
  }

  /**
   * afiseaza un mesaj pentru decani daca se afla in afara celor doua perioada de precazare
   */
  accesInterzisView() {
    return (
      <Container textAlign="center">
        <Segment>
          <Header as="h3">
            Accesul este permis doar în faza I (perioada{" "}
            {this.state.etapeCazare[0][0].toLocaleDateString("ro-RO")} {" - "}
            {this.state.etapeCazare[0][1].toLocaleDateString("ro-RO")}) și în
            faza a II-a (perioada{" "}
            {this.state.etapeCazare[1][0].toLocaleDateString("ro-RO")} {" - "}
            {this.state.etapeCazare[1][1].toLocaleDateString("ro-RO")})
          </Header>
        </Segment>
      </Container>
    );
  }

  /**
   *@param student
   * in momentul in care se executa decazarea unui student, acesta este adaugat in listaStudenti pentru a fi disponibil efectuarii unei posibile noi cazari
   */

  adaugaStudentDecazat = (student, response) => {
    /**
     * actualizarea campurilor studentului atunci cand este decazat cu raspunsul primit din baza de date
     */
    student.CazareConfirmata = false;
    student.ID_Cazare = -1;
    student.CazareExpirataLaDataCurenta = null;
    student.ID_CameraUltimaCazare = response.ID_CameraUltimaCazare;
    student.ID_CaminUltimaCazare = response.ID_CaminUltimaCazare;
    student.NumarCameraUltimaCazare = response.NumarCameraUltimaCazare;
    student.DenumireCaminUltimaCazare = response.DenumireCaminUltimaCazare;
    student.EtajUltimaCazare = response.EtajUltimaCazare;

    if (!this.state.listaStudenti.includes(student)) {
      student.StudentCazatDeja = false;
      let listaStudentiActualizata = []
        .concat(student)
        .concat(this.state.listaStudenti);
      this.setState({listaStudenti: listaStudentiActualizata});
    }
  };

  /**
   *
   * @param tipCameraMF
   * @param ID_Camera
   * actualizeaza icon-ul de gen al camerei
   */
  actualizeazaTipCameraMF = (tipCameraMF, ID_Camera) => {
    this.setState({
      tipCameraMFCazare: tipCameraMF,
      ID_CameraCazare: ID_Camera,
    });
  };

  /**
   *
   * metoda actualizeaza in interfata informatiile din labels despre cazari
   */
  actualizeazaLabels = () => {
    axiosRef
      .get(
        `Cazare/CazareRaportLocuri?ID_AnUniv=${this.state.idAnCazare}&ID_Facultate=${this.state.idFacultate}&ID_Camin=-1`
      )
      .then((r) => {
        this.setState({
          nrLocuriAlocate: r.data.NrTotalLocuriAlocate,
          nrLocuriOcupate: r.data.NrStudentiCazati,
          nrLocuriLibereFete: r.data.NrLocuriLibereFete,
          nrLocuriLibereBaieti: r.data.NrLocuriLibereBaieti,
          nrLocuriLibereNespecificat: r.data.NrLocuriLibereGenNespecificat,
        });
      });
  };

  deschideGhidDecanat = () => {
    window.open(pathGhidUtilizare, "_black");
  };

  handleClickCazareVara = (e, {checked}) => {
    //daca checkbox-ul e bifat se selecteaza tipul de cazare pe vara, altfel se seteaza cazare normala
    let tipCazare = checked ? 2 : 1;

    //se seteaza cookie cu selectia
    setCookie("ID_TipCazare", tipCazare)
    this.setState({ID_TipCazare: tipCazare})
    this.getCazareInfo(this.state.idFacultate, this.state.eSefAdminCamine, tipCazare)
  }

  /**
   * afisarea obisnuita a datelor
   */
  contentView() {
    let listaStudentiFaraCazare = this.state.listaStudenti;

    return (
      /**
       * Pagina de Decanat este impartita in doua randuri -> partea de header -> trei coloane, doua randuri: sigla, titlul, contact+buton Descarca rapoarte, labels
       * Pagina de Decanat este impartita in doua randuri -> partea de header -> trei coloane, doua randuri: sigla, titlul, contact+buton Descarca rapoarte, labels
       *                                                                      -> dropdown 'Alege facultate' pentru eSefAdminCamine=true sau listaFacultati > 1
       *                                                  -> partea de studenti, respectiv camere
       */

      <Grid rows={3} columns="equal">
        <Grid.Row
          columns="equal"
          style={{paddingBottom: "0px"}}
          verticalAlign="middle"
        >
          {/* cod comentat lasat pentru cand gasim o solutie pentru sigle*/}

          {/*<Grid.Column width={4}>*/}
          {/*    <Image*/}
          {/*        className="siglaFacultate"*/}
          {/*        src={*/}
          {/*            window.location.protocol +*/}
          {/*            "//" +*/}
          {/*            window.location.hostname +*/}
          {/*            (window.location.port ? ":" + window.location.port : "") +*/}
          {/*            (this.state.eSefAdminCamine*/}
          {/*                ? "/Images/UT.png"*/}
          {/*                : `/Images/${this.getSiglaFacultate()}.png`)*/}
          {/*        }*/}
          {/*        style={{height: "100%"}}*/}
          {/*    />*/}
          {/*</Grid.Column>*/}

          <Grid.Column textAlign="left" verticalAlign="middle">
            <div
              style={{
                width: "30%",
              }}
            >
              <Menu vertical color="green" inverted className="menuDescarca">
                <Dropdown text="Descarcă" item icon="download">
                  <Dropdown.Menu>
                    <Dropdown.Item
                      type="button"
                      text="Descarcă Ghid"
                      onClick={this.deschideGhidDecanat} //calea ghidului de utilizare pagina Decanat
                    />
                    <MeniuRapoarte
                      RAP_StudentiCazareCuMedie={true}
                      RAP_StudentiCazarePentruAvizier={true}
                      RAP_StudentiNeconfirmatiByFacultate
                      RAP_CamereLibereByFacultate
                      ID_Facultate={this.state.idFacultate}
                      ID_AnUnivCurent={this.state.idAnUniversitarCurent}
                      ID_AnUnivCazare={this.state.idAnCazare}
                      RAP_SituatieOcupareCamine={this.state.eSefAdminCamine}
                      RAP_CereriRamanereCamin
                      eSefAdminCamine={this.state.eSefAdminCamine}
                    />
                  </Dropdown.Menu>
                </Dropdown>
              </Menu>
            </div>

          </Grid.Column>

          <Grid.Column textAlign="center">
            <Header
              as="h2"
              style={{
                marginTop: "10px",
              }}
            ><span>
              {this.state.ID_TipCazare === 2 &&
              <Icon
                name={"sun"}
                color={"yellow"}
              />
              }
              ALOCARE LOCURI {
              this.state.ID_TipCazare === 2
              && <span style={{color: "#fbbd08"}}
              >
                VARA
              </span>

            } - {this.state.anUniversitarPrecazare.Denumire}

              {this.state.ID_TipCazare === 2 &&
              <Icon
                name={"sun"}
                color={"yellow"}
              />
              }
              </span>
            </Header>
            {this.state.eSefAdminCamine ||
            this.state.listaFacultati.length !== 0 ? (
              <Dropdown
                className="dropdownFacultate"
                selection
                search={functieCautareNume}
                placeholder="Alege facultate"
                options={this.state.listaFacultati}
                onChange={this.handleSelecteazaFacultate}
                value={this.state.idFacultate}
              />
            ) : (
              <Header
                as="h3"
                style={{
                  color: this.state.culoareFacultate,
                }}
              >
                {this.state.denumireFacultate.toUpperCase()}
              </Header>
            )}

            {/*selectia tipului de cazare*/}
            {
              (this.state.eSefAdminCamine) && <Checkbox
                onClick={this.handleClickCazareVara}
                label={"Cazeaza studentii ca si CAZARE DE VARA "}
                checked={this.state.ID_TipCazare === 2}
              />
            }

          </Grid.Column>
          <Grid.Column
            textAlign="right"
            style={{marginRight: "5px"}}
            verticalAlign="middle"
          >
            <div style={{marginRight: "5px"}}>
              <Icon name="help" color="orange"/>
              <strong>SUPORT:</strong> <br/>
              tsg@unitbv.ro
            </div>
          </Grid.Column>
        </Grid.Row>

        <Grid.Row>
          <Grid.Column className="labelSeparator"/>
          <Grid.Column className="labelInfo">
            <LabelStatistici
              icon="student"
              title="Număr studenți"
              detail={this.state.listaStudenti.length}
              culoareFacultate={this.state.culoareFacultate}
            />
          </Grid.Column>
          <Grid.Column className="labelSeparator"/>
          <Grid.Column className="labelInfo">
            <LabelStatistici
              icon="file alternate"
              title="Cereri precazare:"
              detail={this.state.nrCereriPrecazare}
              culoareFacultate={this.state.culoareFacultate}
            />
          </Grid.Column>
          <Grid.Column className="labelSeparator"/>
          <Grid.Column className="labelInfo">
            <LabelStatistici
              icon="chart bar"
              title="Locuri alocate:"
              detail={this.state.nrLocuriAlocate}
              culoareFacultate={this.state.culoareFacultate}
            />
          </Grid.Column>
          <Grid.Column className="labelSeparator"/>
          <Grid.Column className="labelInfo">
            <LabelStatistici
              icon="hotel"
              title="Studenți cazați:"
              detail={this.state.nrLocuriOcupate}
              culoareFacultate={this.state.culoareFacultate}
            />
          </Grid.Column>
          <Grid.Column className="labelSeparator"/>
        </Grid.Row>

        <Grid.Row columns={2}>
          <Grid.Column>
            <ListaStudenti
              listaStudenti={listaStudentiFaraCazare}
              idFacultate={this.state.idFacultate}
              culoareFacultate={this.state.culoareFacultate}
              anCurent={this.state.anUniversitarCurent}
              anCazare={this.state.anUniversitarPrecazare}
              actualizeazaLabels={this.actualizeazaLabels.bind(this)}
              schimbaStatusStudent={this.schimbaStatusStudent.bind(this)}
              actualizeazaTipCameraMF={this.actualizeazaTipCameraMF.bind(this)}
              ID_TipCazare={this.state.ID_TipCazare}
              cazareDataDeCand={this.state.cazareDataDeCand}
              cazareDataPanaCand={this.state.cazareDataPanaCand}
            />
          </Grid.Column>
          <Grid.Column>
            <ListaCamere
              eSefAdminCamine={this.state.eSefAdminCamine}
              idFacultate={this.state.idFacultate}
              idAnCazare={this.state.idAnCazare}
              anUniversitarPrecazare={this.state.anUniversitarPrecazare}
              idAnUniversitarCurent={this.state.idAnUniversitarCurent}
              listaCamine={this.state.listaCamine}
              culoareFacultate={this.state.culoareFacultate}
              returneazaStudent={this.returneazaStudent.bind(this)}
              schimbaStatusStudent={this.schimbaStatusStudent.bind(this)}
              nrStudentiCazati={this.state.nrLocuriOcupate}
              nrLocuriLibereFete={this.state.nrLocuriLibereFete}
              nrLocuriLibereBaieti={this.state.nrLocuriLibereBaieti}
              nrLocuriLibereNespecificat={this.state.nrLocuriLibereNespecificat}
              etapeCazare={this.state.etapeCazare}
              cazareDataDeCand={this.state.cazareDataDeCand}
              cazareDataPanaCand={this.state.cazareDataPanaCand}
              actualizeazaTipCameraMF={this.actualizeazaTipCameraMF.bind(this)}
              actualizeazaLabels={this.actualizeazaLabels.bind(this)}
              adaugaStudentDecazat={this.adaugaStudentDecazat.bind(this)}
              ref={this.refChild}
              ID_TipCazare={this.state.ID_TipCazare}
            />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }

  render() {
    if (!this.state.loadedStudenti || !this.state.loadedCamine) {
      return this.loadingView();
    } else {
      //se verifica daca e sefAdminCamine sau se afla in cele doua perioade de precazare
      let currentDate = new Date(moment());
      if (
        this.state.eSefAdminCamine ||
        (currentDate >= this.state.etapeCazare[0][0] &&
          currentDate <= this.state.etapeCazare[0][1]) ||
        (currentDate >= this.state.etapeCazare[1][0] &&
          currentDate <= this.state.etapeCazare[1][1])
      ) {
        return this.contentView();
      } else return this.contentView();
    }
  }
}

export default PaginaDecanat;
