import axios from "axios";

const instance = axios.create({
  baseURL: "http://193.254.231.119/AGSIS.API/",
  headers: { "content-type": "application/json;charset=UTF-8" },
});

export default instance;
